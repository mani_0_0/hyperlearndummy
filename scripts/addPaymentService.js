const paymentService = require('../models/paymentService');
const connectDB = require('../config/db.js');

connectDB();
const main = async () => {
  try {
    // eslint-disable-next-line new-cap
    const newPaymentService = new paymentService({
      serviceCode: (process.argv[2]),
      isActive: Number(process.argv[3]),
    });
    await newPaymentService.save();
    // eslint-disable-next-line no-console
    console.log('Payment Service Added');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log('Error occured');
  }
  // eslint-disable-next-line new-cap
};

main();
