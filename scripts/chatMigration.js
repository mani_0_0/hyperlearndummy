const Message = require('../models/Message');
const Enrolled = require('../models/Enrolled');
const connectDB = require('../config/db.js');

connectDB();
const main = async () => {
  try {
    const enrollments = await Enrolled.find({}).populate([{ path: 'course', select: ['user', 'heading'] }, { path: 'user', select: 'name' }]);
    for (let i = 0; i < enrollments.length; i += 1) {
      const newMessage = new Message({
        senderId: enrollments[i].course.user.toString(),
        receiverId: enrollments[i].user.id.toString(),
        courseId: enrollments[i].course.id.toString(),
        msgType: 'system',
      });
      newMessage.source.msg = `${enrollments[i].user.name} has enrolled in ${enrollments[i].course.heading}`;
      // eslint-disable-next-line no-await-in-loop
      await newMessage.save();
    }
    // eslint-disable-next-line no-console
    console.log('Migration done successfully.');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
  // await db.authenticate();
};

main();
