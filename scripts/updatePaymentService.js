const paymentService = require('../models/paymentService');
const connectDB = require('../config/db.js');

connectDB();
const main = async () => {
  // eslint-disable-next-line new-cap
  const service = await paymentService.findOne({
    serviceCode: (process.argv[2]),
  });
  service.isActive = Number(process.argv[3]);
  await service.save();
};

main();
