const Message = require('../models/Message');
const Enrolled = require('../models/Enrolled');
const connectDB = require('../config/db.js');

connectDB();
const main = async () => {
  try {
    // eslint-disable-next-line new-cap
    const messages = await Message.find();

    for (let i = 0; i < messages.length; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      const enrollment1 = await Enrolled.findOne(
        {
          course: messages[i].courseId,
          user: messages[i].senderId,
          instructorId: messages[i].receiverId,
        },
      );

      if (enrollment1) {
        messages[i].enrollmentId = enrollment1.id;
        // eslint-disable-next-line no-await-in-loop
        await messages[i].save();
        // eslint-disable-next-line no-continue
        continue;
      }

      // eslint-disable-next-line no-await-in-loop
      const enrollment2 = await Enrolled.findOne(
        {
          course: messages[i].courseId,
          user: messages[i].receiverId,
          instructorId: messages[i].senderId,
        },
      );

      if (enrollment2) {
        messages[i].enrollmentId = enrollment2.id;
        // eslint-disable-next-line no-await-in-loop
        await messages[i].save();
        // eslint-disable-next-line no-continue
        continue;
      }
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
  // eslint-disable-next-line new-cap

  console.log('Done')
};

main();
