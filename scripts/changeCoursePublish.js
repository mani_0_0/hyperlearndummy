const Course = require('../models/Course');
const connectDB = require('../config/db.js');

connectDB();
const main = async () => {
  try {
    const courses = await Course.find();
    for (let i = 0; i < courses.length; i += 1) {
      if (courses[i].publish) {
        courses[i].publish = 'published';
      } else {
        courses[i].publish = 'unpublished';
      }

      // eslint-disable-next-line no-await-in-loop
      await courses[i].save();
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
};

main();
