const Module = require('../models/Module');
const connectDB = require('../config/db.js');

connectDB();
const main = async () => {
  try {
    const modules = await Module.find().populate('chapter');
    for (let i = 0; i < modules.length; i += 1) {
      const module = modules[i];
      const courseId = module.chapter.course;
      module.courseId = courseId;
      // eslint-disable-next-line no-await-in-loop
      await module.save();
    }
    // eslint-disable-next-line no-console
    console.log('Migration done successfully.');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
  // await db.authenticate();
};

main();
