const mongoose = require('mongoose');

const Order = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    paymentId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'payment',
      default: null,
    },
    entityType: {
      type: String,
      default: '',
      enum: ['course', 'session_booking'],
    },
    entityId: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    price: {
      type: Number,
      default: 0,
    },
    total: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('Order', Order);
