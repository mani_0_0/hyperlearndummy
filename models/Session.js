const mongoose = require('mongoose');

const Session = new mongoose.Schema(
  {

    mentorId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    sessionType: {
      type: String,
      enum: ['one-on-one', 'group'],
      default: 'one-on-one',
    },
    name: {
      type: String,
      default: '',
    },
    description: {
      type: String,
      default: '',
    },
    duration: {
      // In minutes
      type: Number,
      default: 0,
    },
    price: {
      type: Number,
      default: 0,
    },
    markedPrice: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('session', Session);
