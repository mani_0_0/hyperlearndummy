const mongoose = require('mongoose');

const Booking = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    mentorId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    startTime: {
      type: Date,

    },
    endTime: {
      type: Date,

    },
    comments: {
      type: String,
      default: '',
    },
    status: {
      type: String,
      enum: ['pending', 'failed', 'completed'],
    },
    orderId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'order',
      default: null,
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('booking', Booking);
