const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      unique: true,
    },
    shortBio: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      default: '',
    },
    languages: {
      type: [String],
    },
    introduction: {
      type: String,
      default: '',
    },
    introVideo: {
      type: String,
      default: '',
    },
    skills: {
      type: [String],
    },
    organisations: {
      type: [{ name: { type: String }, url: { type: String }, domain: { type: String } }],
    },
    expInYears: {
      type: Number,
      default: 0,
      required: true,
    },
    website: {
      type: String,
    },
    location: {
      type: String,
    },
    timeZone: {
      type: String,
    },
    bookSession: {
      type: Boolean,
      default: false,
    },
    featured: {
      type: [String],
    },
    verified: {
      type: Boolean,
      default: false,
    },
    profilePic: {
      url: {
        type: String,
        default: '',
        required: true,
      },
      size: {
        type: Number,
        default: 0,
      },
      type: {
        type: String,
        default: '',
      },
      fileName: {
        type: String,
        default: '',
      },
    },
    socials: {
      instagram: {
        type: String,
        default: '',
      },
      twitter: {
        type: String,
        default: '',
      },
      linkedin: {
        type: String,
        default: '',
      },
      discord: {
        type: String,
        default: '',
      },
      github: {
        type: String,
        default: '',
      },
      behance: {
        type: String,
        default: '',
      },
      youtube: {
        type: String,
        default: '',
      },
    },
    activeSocials: {
      type: [String],
      enums: ['twitter', 'instagram', 'facebook', 'behance', 'youtube', 'reddit', 'github', 'linkedin'],
    },
    announcementEnabled: {
      type: Boolean,
    },
    announcement: {
      type: String,
      default: '',
    },
    announcementCTA: {
      type: String,
      default: '',
    },
    onBoardingV1: {
      profile: { type: Boolean, default: false },
      firstSession: { type: Boolean, default: false },
      calendarConnect: { type: Boolean, default: false },
      availability: { type: Boolean, default: false },
      skip: { type: Boolean, default: false },
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('profile', ProfileSchema);
