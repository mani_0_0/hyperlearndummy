const mongoose = require('mongoose');

const Request = new mongoose.Schema(
  {
    requestedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    entityType: {
      type: String,
      default: '',
      enum: ['publishCourse', 'unpublishCourse'],
    },
    entityId: {
      type: String,
      default: '',
    },
    reviewedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      default: null,
    },
    comments: {
      type: String,
      default: '',
    },
    status: {
      type: String,
      enum: ['accepted', 'rejected', 'pending'],
      default: 'pending',
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('request', Request);
