const mongoose = require('mongoose');

const paymentService = new mongoose.Schema(
  {
    serviceCode: {
      type: String,
      default: '',
      unique: true,
      required: true,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('paymentService', paymentService);
