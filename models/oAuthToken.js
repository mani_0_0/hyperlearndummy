const mongoose = require('mongoose');

const oAuthToken = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    oAuthtoken: {
      access_token: {
        type: String,
        default: '',
      },
      expires_in: {
        type: Number,
        default: 0,
      },
      scope: {
        type: String,
        default: '',
      },
      token_type: {
        type: String,
        default: 'Bearer',
      },
      refresh_token: {
        type: String,
        default: '',
      },
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('oAuthToken', oAuthToken);
