const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      default: '',
    },
    phone: {
      type: String,
      required: true,
      unique: true,
    },
    user_type: {
      type: String,
      enum: ['mentor', 'student', 'admin'],
      default: 'student',
    },
    emailVerified: {
      type: Boolean,
      default: false,
    },
    profile: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'profile',
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('user', UserSchema);
