const mongoose = require('mongoose');

const Instructor = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    course: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'course',
    },
    status: {
      type: String,
      default: 'pending',
      enum: ['accepted', 'rejected', 'pending'],
    },
    isActive: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('Instructor', Instructor);
