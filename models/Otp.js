const mongoose = require('mongoose');

const Otp = new mongoose.Schema(
  {
    otp: {
      type: Number,
      required: true,
    },
    retryAttempts: {
      type: Number,
      default: 0,
    },
    generatedFrom: {
      type: String,
      default: 'email',
      enum: ['email', 'phone'],
    },
    source: {
      type: String,
      default: '',
    },
    isUsed: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('otp', Otp);
