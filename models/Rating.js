const mongoose = require('mongoose');

const Rating = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    entityType: {
      type: String,
      default: '',
      enum: ['course'],
    },
    entityID: {
      type: String,
      default: '',
    },
    value: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('rating', Rating);
