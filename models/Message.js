const mongoose = require('mongoose');

const Message = new mongoose.Schema(
  {
    senderId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    receiverId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    courseId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'course',
    },
    enrollmentId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Enrolled',
    },
    msgType: {
      type: String,
      default: '',
      enum: ['text', 'file', 'system'],
    },
    edited: {
      type: Boolean,
      default: false,
    },
    important: {
      type: Boolean,
      default: false,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    seen: {
      type: Boolean,
      default: false,
    },
    source: {
      msg: {
        type: String,
        default: '',
      },
      size: {
        type: Number,
        default: 0,
      },
      type: {
        type: String,
        default: '',
      },
      fileName: {
        type: String,
        default: '',
      },
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('message', Message);
