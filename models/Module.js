const mongoose = require('mongoose');

const moduleSchema = new mongoose.Schema(
  {
    courseId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'course',
    },
    chapter: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'chapter',
    },
    heading: {
      type: String,
      default: '',
    },
    description: {
      type: String,
      default: '',
    },
    moduleType: {
      type: String,
      enum: ['instruction', 'assignment', 'live-session'],
      required: true,
    },
    time: {
      type: String,
      default: '0m',
    },
    source: {
      url: {
        type: String,
        default: '',
      },
      size: {
        type: Number,
        default: 0,
      },
      type: {
        type: String,
        default: '',
      },
      fileName: {
        type: String,
        default: '',
      },
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('module', moduleSchema);
