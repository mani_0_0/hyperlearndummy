const mongoose = require('mongoose');

const accountSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    accountNumber: {
      type: String,
      default: '',
    },
    ifsc: {
      type: String,
      default: '',
    },
    upi: {
      type: String,
      default: '',
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('account', accountSchema);
