const mongoose = require('mongoose');

const Payment = new mongoose.Schema(
  {
    payeeId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    buyerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    paymentMode: {
      type: String,
    },
    paymentModeId: {
      type: String,
    },
    receipt: {
      type: String,
      default: '',
    },
    amount: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      enum: ['pending', 'completed', 'cancelled', 'failed'],
      default: 'pending',
    },
    additionalDetails: {
      type: mongoose.Schema.Types.Mixed,
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('Payment', Payment);
