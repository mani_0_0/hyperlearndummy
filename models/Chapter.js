const mongoose = require('mongoose');

const chapterSchema = new mongoose.Schema(
  {
    course: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'course',
      required: true,
    },
    heading: {
      type: String,
      default: '',
    },
    description: {
      type: String,
      default: '',
    },
    locked: {
      type: Boolean,
      default: false,
    },
    time: {
      type: String,
      default: '0m',
    },
    modules: [{ type: mongoose.Schema.Types.ObjectId, ref: 'module' }],
  },
  { timestamps: true },
);

module.exports = mongoose.model('chapter', chapterSchema, 'chapter');
