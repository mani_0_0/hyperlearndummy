const mongoose = require('mongoose');

const assignmentSchema = new mongoose.Schema(
  {
    enrollmentId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Enrolled',
    },
    moduleId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'module',
    },
    courseId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'course',
    },
    reviewedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      default: '',
    },
    source: {
      url: {
        type: String,
        default: '',
      },
      size: {
        type: Number,
        default: 0,
      },
      type: {
        type: String,
        enum: ['link', 'file'],
        default: '',
      },
      fileName: {
        type: String,
        default: '',
      },
    },
    comments: {
      type: String,
      default: '',
    },
    status: {
      type: Number,
      enum: [0, 1],
    },
    score: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true },
);
module.exports = mongoose.model('assignment', assignmentSchema);
