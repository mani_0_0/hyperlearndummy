const mongoose = require('mongoose');

const Availability = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
    isAvailable: {
      type: Boolean,
      default: false,
    },
    bookingAllowedBefore: {
      type: Number,
      default: 0,
    },
    bookingAllowedTill: {
      type: Number,
      default: 0,
    },
    timeZone: {
      type: String,
      default: '',
    },
    days: {
      0: {
        slots: [{ startTime: { type: Date }, endTime: { type: Date } }],
        isAvailable: { type: Boolean, default: true },
      },
      1: {
        slots: [{ startTime: { type: Date }, endTime: { type: Date } }],
        isAvailable: { type: Boolean, default: true },
      },
      2: {
        slots: [{ startTime: { type: Date }, endTime: { type: Date } }],
        isAvailable: { type: Boolean, default: true },
      },
      3: {
        slots: [{ startTime: { type: Date }, endTime: { type: Date } }],
        isAvailable: { type: Boolean, default: true },
      },
      4: {
        slots: [{ startTime: { type: Date }, endTime: { type: Date } }],
        isAvailable: { type: Boolean, default: true },
      },
      5: {
        slots: [{ startTime: { type: Date }, endTime: { type: Date } }],
        isAvailable: { type: Boolean, default: true },
      },
      6: {
        slots: [{ startTime: { type: Date }, endTime: { type: Date } }],
        isAvailable: { type: Boolean, default: true },
      },
    },
  },
  { timestamps: true },
);

module.exports = mongoose.model('availability', Availability);
