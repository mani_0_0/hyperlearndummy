const mongoose = require('mongoose');

const Enrolled = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
    course: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'course',
      required: true,
    },
    modules: [{
      module: { type: mongoose.Schema.Types.ObjectId, ref: 'module' }, isWatched: { type: Boolean, default: false }, isSaved: { type: Boolean, default: false }, input: { type: String, default: '' },
    }],
    instructorId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
  },
  { timestamps: true },
);
Enrolled.index({ user: 1, course: 1 }, { unique: true });
module.exports = mongoose.model('Enrolled', Enrolled);
