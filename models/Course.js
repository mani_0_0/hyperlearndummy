const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
    },
    heading: {
      type: String,
      required: true,
    },
    subheading: {
      type: String,
      default: '',
    },
    description: {
      type: String,
      default: '',
    },
    price: {
      type: Number,
      default: 0,
      required: true,
    },
    markedPrice: {
      type: Number,
      default: 0,
    },
    publish: {
      type: String,
      enum: ['unpublished', 'published', 'pending'],
      default: 'unpublished',
    },
    rating: {
      type: String,
      default: '0',
    },
    freeSessions: {
      type: Number,
      default: 0,
    },
    source: {
      url: {
        type: String,
        default: '',
      },
      size: {
        type: Number,
        default: 0,
      },
      type: {
        type: String,
        default: '',
      },
      fileName: {
        type: String,
        default: '',
      },
    },
    courseLengthInHrs: {
      type: Number,
      default: '',
    },
    language: {
      type: String,
      default: 'English',
    },
    subtitles: {
      type: Boolean,
      default: true,
    },
    lifeTimeAccess: {
      type: Boolean,
      default: true,
    },
    certificateOfCompletion: {
      type: Boolean,
      default: true,
    },
    individualSessions: {
      type: Boolean,
      default: true,
    },
    courseValidity: {
      type: Number,
      default: 0,
    },
    featured: [{ type: String }],
    chapters: [{ type: mongoose.Schema.Types.ObjectId, ref: 'chapter' }],
  },
  { timestamps: true },
);
courseSchema.index({
  heading: 'text', description: 'text', subheading: 'text',
}, {
  name: 'users_full_text',
  default_language: 'en',
  language_override: 'en',
});

module.exports = mongoose.model('course', courseSchema, 'course');
