/* eslint-disable no-underscore-dangle */
const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const path = require('path');
const axios = require('axios');
const Promise = require('bluebird');

const app = express();
const redis = require('redis');
const server = require('http').Server(app);
const io = require('socket.io')(server, { pingInterval: 2000, pingTimeout: 5000 });
const mongoose = require('mongoose');

const { config } = require('./config/default');
const Message = require('./models/Message');
const oAuthToken = require('./models/oAuthToken');
const Enrolled = require('./models/Enrolled');
const connectDB = require('./config/db.js');
const Profile = require('./models/Profile');

dotenv.config();
// Set static 
app.use(express.static(path.join(__dirname, 'public')));

const client = redis.createClient({
  host: config.redisClient,
  port: Number(config.redisPort),
  password: config.redisPassword,
});

// Run when client connects
io.on('connection', (socket) => {
  socket.on('joinServer', async ({ username }) => {
    try {
      // eslint-disable-next-line no-unused-vars
      client.hmset('userToSocket', username, socket.id, async (err, response) => {
        // eslint-disable-next-line no-console
        if (err) console.log('Error on user to socket ', err);
      });
      // eslint-disable-next-line no-unused-vars
      client.hmset('socketToUser', socket.id, username, async (err, response) => {
        // eslint-disable-next-line no-console
        if (err) console.log('Error on scoket to user ', err);
      });
      // eslint-disable-next-line no-unused-vars
      client.sadd('online', username, async (err, response) => {
        // eslint-disable-next-line no-console
        if (err) console.log('Error on set online ', err);
      });

      socket.join(username);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  });

  // Listen for chatMessage
  socket.on('chatMessage', async ({
    msg, sender, receiver, courseId, instructorId, enrollmentId,
  }) => {
    try {
      if (receiver) {
        const newMessage = new Message({
          senderId: sender, receiverId: receiver, msgType: 'text', courseId, enrollmentId,
        });
        newMessage.source.msg = msg;
        await newMessage.save();

        let receiverSocket;
        let status = 'offline';
        client.SISMEMBER('online', receiver, async (err, response) => {
          if (response) {
            status = 'online';
            client.hmget('userToSocket', receiver, (_err, resp) => {
              if (resp) {
                if (resp[0] !== null) {
                  // eslint-disable-next-line prefer-destructuring
                  receiverSocket = resp[0];
                  io.to(receiverSocket).emit('message', { newMessage, status, instructorId });
                }
              }
            });
          }
        });

        io.to(socket.id).emit('message', { newMessage, status, instructorId });
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  });

  socket.on('deleteChatMessage', async ({
    // eslint-disable-next-line no-unused-vars
    msgId, receiver, sender, instructorId,
  }) => {
    try {
      const message = await Message.findById(msgId);
      if (message) {
        message.isDeleted = true;
        await message.save();
        let receiverSocket;
        // save and send message here accordingly
        client.SISMEMBER('online', receiver, async (err, response) => {
          if (response) {
            client.hmget('userToSocket', receiver, (_err, resp) => {
              if (resp) {
                if (resp[0] !== null) {
                  // eslint-disable-next-line prefer-destructuring
                  receiverSocket = resp[0];
                  io.to(receiverSocket).emit('deleteMessage', { message, instructorId });
                }
              }
            });
          }
        });

        io.to(socket.id).emit('deleteMessage', { message, instructorId });
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  });

  socket.on('seenChatMessage', async ({
    enrollmentId, time,
  }) => {
    try {
      const messages = await Message.find({ createdAt: { $gte: time }, enrollmentId, seen: false });

      for (let i = 0; i < messages.length; i += 1) {
        messages[i].seen = true;
        // eslint-disable-next-line no-await-in-loop
        await messages[i].save();
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  });

  socket.on('getChat', async ({
    secondUser, enrollmentId, offset, limit,
  }) => {
    try {
      // eslint-disable-next-line max-len
      const messages = await Message.find({ enrollmentId }).sort({ createdAt: -1 }).limit(Number(limit)).skip(Number(offset));
      let status = 'offline';
      client.SISMEMBER('online', secondUser, async (err, resp) => {
        if (resp) {
          status = 'online';
        }
        if (messages[0]) {
          if (messages[0].senderId.toString() === secondUser.toString()) {
            for (let i = 0; i < messages.length; i += 1) {
              if (messages[i].seen) {
                // eslint-disable-next-line no-continue
                continue;
              } else {
                messages[i].seen = true;
                // eslint-disable-next-line no-await-in-loop
                await messages[i].save();
              }
            }
          }
        }
        io.to(socket.id).emit('sendChat', { messages, status });
      });

      // });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  });

  socket.on('getUnseenMessageCount', async ({ user }) => {
    try {
      const unseenMessagesCount = await Message.countDocuments({
        $or: [
          {
            senderId: { $ne: user },
            receiverId: user,
            seen: false,
          },
          { msgType: 'system', seen: false },
        ],
      });
      io.to(socket.id).emit('sendUnseenMessageCount', { unseenMessagesCount });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  });
  socket.on('getChats', async ({
    senderId, mode,
  }) => {
    try {
      let query = [
        { instructorId: mongoose.Types.ObjectId(senderId) },
        { user: mongoose.Types.ObjectId(senderId) },
      ];
      if (mode === 'student') {
        query = { user: mongoose.Types.ObjectId(senderId) };
      } else if (mode === 'instructor') {
        query = { instructorId: mongoose.Types.ObjectId(senderId) };
      } else {
        query = {
          $or: [
            { instructorId: mongoose.Types.ObjectId(senderId) },
            { user: mongoose.Types.ObjectId(senderId) },
          ],
        };
      }

      const pipeline = [
        {
          $match: query,
        },
        {
          $lookup: {
            from: 'messages',
            let: { e_id: '$_id' },
            pipeline: [

              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$enrollmentId', '$$e_id'] },
                    ],
                  },
                },

              },
              { $sort: { createdAt: -1 } },
            ],
            as: 'message',
          },
        },
        {
          $lookup: {
            from: 'messages',
            let: { e_id: '$_id' },
            pipeline: [

              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$enrollmentId', '$$e_id'] },
                      { $eq: ['$seen', false] },
                    ],
                  },
                },

              },
              { $count: 'unseen_messages' },
            ],
            as: 'unseenMessage',
          },
        },
        {
          $lookup: {
            from: 'course', localField: 'course', foreignField: '_id', as: 'courseDetails',
          },
        },
        {
          $lookup: {
            from: 'profiles', localField: 'user', foreignField: 'user', as: 'profile',
          },
        },
        {
          $lookup: {
            from: 'users', localField: 'user', foreignField: '_id', as: 'userDetails',
          },
        },
        {
          $lookup: {
            from: 'users', localField: 'instructorId', foreignField: '_id', as: 'instructorDetails',
          },
        },
        {
          $lookup: {
            from: 'profiles', localField: 'instructorId', foreignField: 'user', as: 'instructorProfile',
          },
        },
        {
          $project: {
            _id: 1,
            instructor: { id: '$instructorId', name: { $first: '$instructorDetails.name' }, profile: { $first: '$instructorProfile.profilePic' } },
            student: { id: '$user', name: { $first: '$userDetails.name' }, profile: { $first: '$profile.profilePic' } },
            message: { $first: '$message' },
            courseDetails: { id: '$course', heading: { $first: '$courseDetails.heading' }, source: { $first: '$courseDetails.source' } },
            mentor: { $first: '$courseDetails.user' },
            unseenMessages: { $first: '$unseenMessage.unseen_messages' },
          },
        },
      ];
      const chats = await Enrolled.aggregate(pipeline);
      io.to(socket.id).emit('sendChats', chats);

      //   }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  });

  socket.on('disconnect', () => {
    try {
      // eslint-disable-next-line no-console
      client.hmget('socketToUser', socket.id, (err, resp) => {
        if (resp) {
          if (resp[0] !== null) {
            // eslint-disable-next-line no-unused-vars
            client.hdel('userToSocket', resp[0], (error, respo) => {
              if (error) {
                // eslint-disable-next-line no-console
                console.log(error);
              }
            });
            client.srem('online', resp[0], async (_err, response) => {
              // eslint-disable-next-line no-console
              console.log(response);
              io.local.emit('offline', { status: 'offline' });
            });
            client.hdel('socketToUser', socket.id, (error) => {
              if (err) {
                // eslint-disable-next-line no-console
                console.log(error);
              }
            });
          }
        }
      });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error.message);
    }
  });
});

const PORT =  5000;

connectDB();

app.use(express.json({ extended: false }));
app.use(cors());

// Google rdirect URI
// need to be same as in google console config
app.get('/api/token', async (req, res) => {
  try {
    const postDataUrl = `${'https://www.googleapis.com/oauth2/v4/token?'
      + 'code='}${req.query.code
    }&client_id=${config.googleClientId
    }&client_secret=${config.googleClientSecret
    }&redirect_uri=${config.redirectUri
    }&grant_type=authorization_code`;

    const auth = await axios.post(postDataUrl);

    if (req.query.state) {
      const authToken = await oAuthToken({ userId: req.query.state });
      if (authToken) {
        // eslint-disable-next-line no-obj-calls
        authToken.oAuthtoken = auth.data;
        await authToken.save();
        return res.redirect(config.successOAuthRedirect);
      }

      // eslint-disable-next-line new-cap
      const newOauth = new oAuthToken({
        userId: req.query.state,
        // eslint-disable-next-line no-obj-calls
        oAuthtoken: auth.data,
      });

      const profile = await Profile.findOne({ user: req.user.id });
      profile.onBoardingV1.calendarConnect = true;
      const saveOAuth = newOauth.save();
      const saveProfile = profile.save();
      // eslint-disable-next-line no-unused-vars
      const resp = await Promise.all([saveOAuth, saveProfile]);
      return res.redirect(config.successOAuthRedirect);
    }

    return res.status(400).send('Bad request');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server  jError');
  } 
});

app.get('/', (res) => { res.send('Server is running'); });

app.use('/api/course', require('./routes/api/courses'));
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/course', require('./routes/api/modules'));
app.use('/api/course', require('./routes/api/chapters'));
app.use('/api/enroll', require('./routes/api/enrolled'));
app.use('/api/instructor', require('./routes/api/instructor'));
app.use('/api/rating', require('./routes/api/rating'));
app.use('/api/assignment', require('./routes/api/assignments'));
app.use('/api/students', require('./routes/api/students'));
app.use('/api/payment', require('./routes/api/payment'));
app.use('/api/request', require('./routes/api/request'));
app.use('/api/scheduler', require('./routes/api/scheduler'));
app.use('/api/onboarding', require('./routes/api/onboarding'));
app.use('/api/utils', require('./routes/api/utils'));

server.listen(PORT, async () => {
  // eslint-disable-next-line no-console
  console.log(`Server is up  running ${PORT}`);
});
