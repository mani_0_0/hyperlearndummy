const express = require('express');

const router = express.Router();
const { check } = require('express-validator');
const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  getAvailability,
  getBookings,
  makeBooking,
  setAvailability,
  googleOAuthLoginLink,
  checkToken,
  createSession,
  getSessions,
} = require('../../controllers/scheduler.controller');

router.post('/booking/user/:userId', [auth], getBookings);

router.get('/availability/mentor/:mentorId', [auth], getAvailability);

router.post('/booking', [auth], makeBooking);

router.get('/connectgoogle', [auth, isAuthorized], googleOAuthLoginLink);

router.post('/availability', [auth, isAuthorized], setAvailability);

router.get('/mentor/:mentorId', checkToken);

router.get('/session', [auth], getSessions);

router.post('/session',
  [
    auth,
    isAuthorized,
    check('name', 'Please Enter Name').not().isEmpty(),
    check('duration', 'Please Enter a valid duration').not().isEmpty(),
    check('price', 'Please Enter Price').not().isEmpty(),
    check('markedPrice', 'Please Enter Marked Price').not().isEmpty(),
  ], createSession);
module.exports = router;
