const express = require('express');

const router = express.Router();

const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  sendInvite,
  showInvites,
  getInstructors,
  getSharedCourses,
  searchInstructor,
  inviteToCourse,
} = require('../../controllers/instructors.controller');

router.post('/course/:courseid', [auth], sendInvite);

router.put('/:instructorId', [auth, isAuthorized], inviteToCourse);

router.get('/requests', [auth, isAuthorized], showInvites);

router.get('/', [auth], getInstructors);

router.get('/courses/shared', [auth], getSharedCourses);

router.get('/user', [auth], searchInstructor);

module.exports = router;
