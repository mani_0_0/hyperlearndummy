const express = require('express');

const router = express.Router();
const upload = require('../../middleware/multerMiddleware');
const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  createCourse,
  getCourseById,
  getAllCourses,
  editCourse,
  deleteCourse,
  getMyCourses,
  getMyCreatedCourses,
} = require('../../controllers/courses.controller');

// POST api/course
router.post(
  '/',
  [
    auth,
    isAuthorized,
    upload.array('source')],
  createCourse,
);

// GET api/courses
router.get('/all', getAllCourses);

// GET api/Courses/:id
router.get('/:courseid', getCourseById);

// put /api/edit/:id
router.put('/:courseid', [auth, isAuthorized, upload.array('source')], editCourse);

// delete /api/delete/:id
router.delete('/:courseid', [auth, isAuthorized], deleteCourse);

// get my courses
router.get('/enrolled/courses', [auth], getMyCourses);

// my created
router.get('/created/courses', [auth, isAuthorized], getMyCreatedCourses);

module.exports = router;
