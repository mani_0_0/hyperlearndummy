const express = require('express');

const router = express.Router();
const { check } = require('express-validator');

const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  createChapter,
  editChapter,
  deleteChapter,
} = require('../../controllers/chapters.controller');

router.post(
  '/:courseid/chapter',
  [
    auth,
    isAuthorized,
    check('heading', 'Please Make a Heading').not().isEmpty(),
  ],
  createChapter,
);

router.put(
  '/:courseid/chapter/:chapterid',
  [
    auth,
    isAuthorized,
    check('heading', 'Please Make a Heading').not().isEmpty(),
  ],
  editChapter,
);

router.delete('/:courseid/chapter/:chapterid', [auth, isAuthorized], deleteChapter);

module.exports = router;
