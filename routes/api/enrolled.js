const express = require('express');

const router = express.Router();

const auth = require('../../middleware/auth');

const {
  newEnrollment,
  moduleWatched,
  moduleSaved,
  getEnrollment,
} = require('../../controllers/enrollment.controller');

router.post('/', [auth], newEnrollment);

router.post('/:enrollmentid/watched', [auth], moduleWatched);

router.post('/:enrollmentid/save', [auth], moduleSaved);

router.get('/enrollment', [auth], getEnrollment);

// router.post('/unenroll', [auth], unenroll);
module.exports = router;
