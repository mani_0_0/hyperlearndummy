const express = require('express');

const router = express.Router();

const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  viewAssignments, getModuleAssignments, reviewAssignments, submitAssignment,
} = require('../../controllers/assignments.controller');

router.get('/enrollment/:enrollmentId', [auth], viewAssignments);

router.post('/', [auth], submitAssignment);

router.get('/enrollment/:enrollmentId/module/:moduleId', [auth], getModuleAssignments);

router.put('/:assignmentId', [auth, isAuthorized], reviewAssignments);

module.exports = router;
