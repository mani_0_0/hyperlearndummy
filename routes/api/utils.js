const express = require('express');

const router = express.Router();
const auth = require('../../middleware/auth');

const { companyAutoSearch } = require('../../controllers/onboarding.controller');

router.get('/companySearch', auth, companyAutoSearch);

module.exports = router;
