const express = require('express');

const router = express.Router();
const { check } = require('express-validator');
const upload = require('../../middleware/multerMiddleware');
const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  createModule,
  deleteModule,
  editModule,
} = require('../../controllers/modules.controller');

router.post(
  '/:courseid/chapter/:chapterid/module',
  [
    auth,
    isAuthorized,
    // check('heading', 'Please Make a Heading').not().isEmpty(),
    // check('description', 'Please Make a Description').not().isEmpty(),
    // check('source', 'Please Make a Source').not().isEmpty(),
    upload.array('source'),
  ],
  createModule,
);

router.delete('/:courseid/chapter/:chapterid/module/:moduleid', [auth, isAuthorized], deleteModule);

router.put(
  '/:courseid/chapter/:chapterid/module/:moduleid',
  [
    auth,
    isAuthorized,
    check('heading', 'Please Make a Heading').not().isEmpty(),
    check('description', 'Please Make a Description').not().isEmpty(),
    upload.array('source'),
  ],
  editModule,
);
module.exports = router;
