const express = require('express');

const router = express.Router();
const { check } = require('express-validator');
const upload = require('../../middleware/multerMiddleware');
const auth = require('../../middleware/auth');
const {
  verifyUser,
  userSignup,
  sendEmailVerification,
  newProfile,
  editProfile,
  checkEmailVerification,
  viewProfile,
  sendEmailOtp,
  sendPhoneOtp,
  getRating,
} = require('../../controllers/users.controller');

// Email OTP
router.post(
  '/email/verify',
  [auth],
  sendEmailVerification,
);

router.get(
  '/email/verify/user/:userid',
  checkEmailVerification,
);
// POST api/users/verify
router.post(
  '/verify',
  [
    check('phone', 'Enter phone of atleast  length of 10').isLength({
      min: 10,
    }),
  ],
  verifyUser,
);

// POST api/users/signup
router.post(
  '/signup',
  [
    check('name', 'Please Enter Name').not().isEmpty(),
    check('email', 'Please Enter a valid email').isEmail(),
  ],
  userSignup,
);

router.post('/email/otp', sendEmailOtp);
router.post('/phone/otp', sendPhoneOtp);
router.post('/profile', [auth, upload.array('image')], newProfile);
router.put('/profile', [auth, upload.array('image')], editProfile);
router.get('/:userid/profile', viewProfile);
router.get('/rating', [auth], getRating);
module.exports = router;
