const express = require('express');

const router = express.Router();

const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  getRequests,
  reviewRequest,
  makeRequest,
} = require('../../controllers/requests.controller');
const isAdmin = require('../../middleware/isAdmin');
const adminOrMentor = require('../../middleware/adminOrMentor');

router.get('/', [auth, adminOrMentor], getRequests);

router.post('/', [auth, isAuthorized], makeRequest);

router.put('/', [auth, isAdmin], reviewRequest);

// router.post('/unenroll', [auth], unenroll);
module.exports = router;
