const express = require('express');

const router = express.Router();

const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  getStudents,
  assignInstructor,
  dashBoardStats,
} = require('../../controllers/students.controller');

router.get('/', [auth], getStudents);

router.post('/assign', [auth, isAuthorized], assignInstructor);

router.get('/stats', auth, dashBoardStats);
module.exports = router;
