const express = require('express');

const router = express.Router();

const { check } = require('express-validator');
const auth = require('../../middleware/auth');

const {
  rateEntity,
  getRating,
} = require('../../controllers/ratings.controller');

router.put('/type/:entityType/id/:entityID', [check('value', 'rating Should be between 1 to 5').isInt({ min: 1, max: 5 }), auth], rateEntity);

router.get('/type/:entityType/id/:entityID', [auth], getRating);
module.exports = router;
