const express = require('express');
const { check } = require('express-validator');

const router = express.Router();
const upload = require('../../middleware/multerMiddleware');
const auth = require('../../middleware/auth');
const isAuthorized = require('../../middleware/isAuthorized');

const {
  firstSession,
  setAvailability,
  createProfile,
  skipOnboarding,
} = require('../../controllers/onboarding.controller');

router.post('/firstSession', [
  auth,
  isAuthorized,
  check('name', 'Please Enter Name').not().isEmpty(),
  check('duration', 'Please Enter a valid duration').not().isEmpty(),
  check('price', 'Please Enter Price').not().isEmpty(),
  check('markedPrice', 'Please Enter Marked Price').not().isEmpty(),
], firstSession);

router.post('/availability', [auth, isAuthorized], setAvailability);

router.post('/profile', [auth, upload.array('image')], createProfile);

router.put('/prompt/:choice', [auth], skipOnboarding);

module.exports = router;
