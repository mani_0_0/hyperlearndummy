const express = require('express');

const router = express.Router();

const auth = require('../../middleware/auth');

const {
  verifyPayment,
  getOrders,
  updateOrder,
  makePayment,
  myEarnings,
  getServices,
} = require('../../controllers/payments.controller');

router.post('/verify', verifyPayment);

router.put('/', updateOrder);

router.post('/', [auth], makePayment);

router.get('/', [auth], getOrders);

router.get('/earnings', [auth], myEarnings);

router.get('/services', [auth], getServices);
module.exports = router;
