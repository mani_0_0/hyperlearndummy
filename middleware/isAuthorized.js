/* eslint-disable consistent-return */
// eslint-disable-next-line func-names
module.exports = function (req, res, next) {
  if (req.user.type !== 'mentor') {
    return res.status(401).json({ msg: 'Acccess Denied' });
  }

  try {
    next();
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(401).json({ msg: 'Error Occured' });
  }
};
