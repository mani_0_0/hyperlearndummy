/* eslint-disable max-len */
const mongoose = require('mongoose');
const Promise = require('bluebird');
const _ = require('lodash');
const Course = require('../models/Course');
const Enrolled = require('../models/Enrolled');
const Message = require('../models/Message');
const Payment = require('../models/Payment');

const getStudents = async (req, res) => {
  try {
    let query;
    let offset = 0;
    let limit = 10;

    if (req.query.offset) {
      offset = Number(req.query.offset);
    }
    if (req.query.limit) {
      limit = Number(req.query.limit);
    }
    if (req.query.mentorId) {
      const courseIds = await Course.find({ user: req.user.id }).select('id');
      const ids = await courseIds.map((e) => mongoose.Types.ObjectId(e.id));
      query = { $or: [{ $expr: { $in: ['$course', ids] } }, { instructorId: mongoose.Types.ObjectId(req.query.mentorId) }] };
    } else if (req.query.instructor && req.query.course) {
      query = { $and: [{ instructorId: mongoose.Types.ObjectId(req.query.instructor.toString()) }, { course: mongoose.Types.ObjectId(req.query.course) }] };
    } else if (req.query.instructor) {
      query = { instructorId: mongoose.Types.ObjectId(req.query.instructor.toString()) };
    } else if (req.query.course) {
      query = { course: mongoose.Types.ObjectId(req.query.course) };
    }

    const pipeline = [
      { $match: query },
      {
        $lookup: {
          from: 'users', localField: 'user', foreignField: '_id', as: 'userDetails',
        },
      },
      {
        $lookup: {
          from: 'course', localField: 'course', foreignField: '_id', as: 'courseDetails',
        },
      },
      {
        $lookup: {
          from: 'users', localField: 'instructorId', foreignField: '_id', as: 'instructorDetails',
        },
      },
      {
        $lookup: {
          from: 'modules',
          let: { c_id: '$course' },
          pipeline: [

            {
              $match: {
                $expr:
                  { $eq: ['$courseId', '$$c_id'] },
              },
            },
            { $count: 'total_modules' },
          ],
          as: 'progressDetails',
        },
      },
      {
        $lookup: {
          from: 'assignments',
          let: { e_id: '$_id' },
          pipeline: [

            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$enrollmentId', '$$e_id'] },
                    { $eq: ['$status', 0] },
                  ],
                },
              },
            },
            { $count: 'pending_assignments' },
          ],
          as: 'assignDetails',
        },
      },
      {
        $project: {
          _id: 1,
          user: 1,
          course: 1,
          email: { $first: '$userDetails.email' },
          phone: { $first: '$userDetails.phone' },
          courseName: { $first: '$courseDetails.heading' },
          userName: { $first: '$userDetails.name' },
          instructorName: { $first: '$instructorDetails.name' },
          pendingAssignments: { $first: '$assignDetails.pending_assignments' },
          progess: { $first: '$progressDetails' },
          watched: '$modules',
        },
      },
    ];

    if (req.query.search) {
      const search = [];
      const rex = new RegExp(req.query.search);
      search.push({ userName: { $regex: rex, $options: 'i' } });
      search.push({ courseName: { $regex: rex, $options: 'i' } });
      search.push({ instructorName: { $regex: rex, $options: 'i' } });
      pipeline.push({ $match: { $or: search } });
    }
    if (req.query.sortBy) {
      const sortInfo = {};
      // this part yet to be implementted
      if (req.query.sortBy === 'pendingAssignments' || req.query.sortBy === 'progress') {
        if (req.query.sortOrder === 'desc') {
          sortInfo[req.query.sortBy] = -1;
        } else {
          sortInfo[req.query.sortBy] = 1;
        }
        pipeline.push({ $sort: sortInfo });
      }
    }
    pipeline.push({
      $facet: {
        paginatedResults: [{ $skip: offset }, { $limit: limit }],
        totalCount: [
          {
            $count: 'count',
          },
        ],
      },
    });

    const result = await Enrolled.aggregate(pipeline);

    const students = await result[0].paginatedResults.map((e) => {
      const { watched } = e;
      let count = 0;
      for (let i = 0; i < watched.length; i += 1) {
        if (watched[i].isWatched === true) {
          count += 1;
        }
      }
      if (e.progess) {
        if (e.progess.total_modules > 0) {
          e.progess = count / e.progess.total_modules;
        } else {
          e.progress = 0;
        }
      }
      delete e.watched;
      return e;
    });
    return res.status(200).json({ students, totalCount: result[0].totalCount });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const assignInstructor = async (req, res) => {
  try {
    const enrollment = await Enrolled.findById(req.body.enrollmentId).populate({ path: 'course', select: 'heading user' });
    if (!enrollment) {
      return res.status(400).json({ msg: 'Invalid Request' });
    }
    if (req.user.id !== enrollment.course.user.toString()) {
      return res.status(400).json({ msg: 'Invalid Request' });
    }
    enrollment.instructorId = req.body.instructor;
    await enrollment.save();
    return res.status(200).json(enrollment);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.status(500).send('Server Error');
  }
};

const dashBoardStats = async (req, res) => {
  try {
    const query = { instructorId: mongoose.Types.ObjectId(req.user.id) };

    const pipeline = [
      { $match: query },
      {
        $lookup: {
          from: 'assignments',
          let: { e_id: '$_id' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$enrollmentId', '$$e_id'] },
                    { $eq: ['$status', 0] },
                  ],
                },
              },
            },
            { $count: 'pending_assignments' },
          ],
          as: 'assignDetails',
        },
      },
      {
        $project: {
          instructorId: 1,
          user: 1,
          course: 1,
          pendingAssignments: { $first: '$assignDetails.pending_assignments' },
        },
      },
      {
        $group: {
          _id: { instructorId: '$instructorId' },
          assignments: { $sum: '$pendingAssignments' },
        },
      },
    ];

    const pendingAssignments = Enrolled.aggregate(pipeline);
    const messages = Message.aggregate([{ $match: { receiverId: mongoose.Types.ObjectId(req.user.id), seen: false } },
      {
        $facet: {
          totalNewMsgs: [{ $count: 'totalNewMsgs' }],
          msgsFromCount: [{ $group: { _id: '$senderId' } },
            { $count: 'msgsFromCount' }],
        },
      }]);

    const date = new Date();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    const earnings = Payment.aggregate([
      {
        $match: { $and: [{ payeeId: mongoose.Types.ObjectId(req.user.id) }, { status: 'completed' }] },
      },
      {
        $group: {
          _id: {
            user: '$payeeId',
            month: {
              $month: '$createdAt',
            },
            year: { $year: '$createdAt' },
          },

          earnings: { $sum: '$amount' },
        },
      },
      {
        $project: {
          user: '$user',
          month: '$_id.month',
          year: '$_id.year',
          earnings: '$earnings',
        },
      },
      {
        $match: { $and: [{ month }, { year }] },
      },
    ]);
    const values = await Promise.all([pendingAssignments, messages, earnings]);

    const result = {};
    if (values[0]) {
      result.pendingAssignments = _.get(values[0][0], 'assignments');
    } else {
      result.pendingAssignments = 0;
    }

    if (values[1]) {
      result.unseenMessages = _.get(values[1][0], 'totalNewMsgs[0].totalNewMsgs', 0);
      result.msgsFromCount = _.get(values[1][0], 'msgsFromCount[0].msgsFromCount', 0);
    } else {
      result.unseenMessages = 0;
      result.msgsFromCount = 0;
    }

    if (values[2]) {
      result.earnings = _.get(values[2][0], 'earnings', 0);
    } else {
      result.earnings = 0;
    }
    return res.status(200).json(result);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.status(500).send('Server Error');
  }
};
module.exports = { getStudents, assignInstructor, dashBoardStats };
