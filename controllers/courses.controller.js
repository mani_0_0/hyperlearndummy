/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
const mongoose = require('mongoose');
const Promise = require('bluebird');
const _ = require('lodash');
const { validationResult } = require('express-validator');
const User = require('../models/User');
const { uploadToS3 } = require('../services/uploadToS3.service');
const Course = require('../models/Course');
const Enrolled = require('../models/Enrolled');
const Rating = require('../models/Rating');
const Instructor = require('../models/Instructor');

const createCourse = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const newCourse = new Course({
      user: req.user.id,
      heading: req.body.heading,
      subheading: req.body.subheading,
      description: req.body.description,
      markedPrice: req.body.markedPrice,
      price: req.body.price,
      courseLengthInHrs: req.body.courseLengthInHrs,
      featured: req.body.featured,
      lifeTimeAccess: req.body.lifeTimeAccess,
      subtitles: req.body.subtitles,
      language: req.body.language,
      certificateOfCompletion: req.body.certificateOfCompletion,
      individualSessions: req.body.individualSessions,
      courseValidity: req.body.courseValidity,
      freeSessions: req.body.freeSessions,
    });
    if (req.body.source) {
      newCourse.source.type = 'link';
      newCourse.source.url = req.body.source;
      newCourse.source.size = 0;
    } else if (req.files[0]) {
      const uploadImage = await uploadToS3(req.files[0].path);
      const url = ((await uploadImage).Location);
      newCourse.source.url = url;
      newCourse.source.size = req.files[0].size;
      newCourse.source.type = req.files[0].mimetype;
      newCourse.source.fileName = req.files[0].originalname;
    }

    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).json({ msg: 'Invalid Request' });
    }

    await newCourse.save();

    return res.json(newCourse);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

// eslint-disable-next-line consistent-return
const getCourseById = async (req, res) => {
  try {
    const Courses = await Course.findById(req.params.courseid).populate([{
      path: 'chapters',
      populate: {
        path: 'modules',
      },
    }, {
      path: 'user',
      select: 'name',
      populate: {
        path: 'profile',
      },
    }]);
    if (!Courses) {
      return res.status(404).json({ msg: 'Course not found' });
    }

    const ratings = Rating.aggregate([{ $match: { entityID: req.params.courseid, entityType: 'course' } }, { $group: { _id: { entityID: '$entityID', entitytype: '$entityType' }, rating: { $avg: '$value' } } }]);
    const instructors = Instructor.find({ course: req.params.courseid, status: 'accepted' }).populate({
      path: 'user',
      select: 'name',
      populate: {
        path: 'profile',
      },
    });
    const numberOfStudents = Enrolled.countDocuments({ course: req.params.courseid });

    const values = await Promise.all([ratings, instructors, numberOfStudents]);

    if (values) {
      let rating = 0;
      if (values[0] && values[0][0]) {
        rating = _.get(values[0][0], 'rating');
      }
      const ratedCourses = {
        ...Courses.toObject(),
        rating,
        instructors: values[1],
        numberOfStudents: values[2],
      };
      return res.json(ratedCourses);
    }

    return res.json(Courses);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    if (err.kind === 'ObjectId') {
      return res.status(404).json({ msg: 'Course not found' });
    }
    return res.status(500).send('Server Error');
  }
};

const getAllCourses = async (req, res) => {
  try {
    const query = [];

    if (req.query.search) {
      const rex = new RegExp(req.query.search);
      query.push({ heading: { $regex: rex, $options: 'i' } });
      query.push({ subheading: { $regex: rex, $options: 'i' } });
      query.push({ description: { $regex: rex, $options: 'i' } });
    } else {
      query.push({});
    }
    const match = { $or: query, publish: 'published' };

    if (req.query.mentorId) {
      match.user = req.query.mentorId;
    }

    const sortInfo = {};
    if (req.query.sortBy === 'price' || req.query.sortBy === 'rating') {
      if (req.query.sortOrder === 'desc') {
        sortInfo[req.query.sortBy] = -1;
      } else {
        sortInfo[req.query.sortBy] = 1;
      }
    }
    let offset = 0;
    if (req.query.offset) {
      offset = Number(req.query.offset);
    }
    let limit = 10;
    if (req.query.limit) {
      limit = Number(req.query.limit);
    }
    const Courses = await Course.find(match)
      .sort({ date: -1 })
      .populate({
        path: 'user',
        select: 'name',
        populate: {
          path: 'profile',
          select: 'profilePic',
        },
      }).skip(Number(offset))
      .limit(Number(limit))
      .sort(sortInfo);

    const ratings = await Rating.aggregate([{ $group: { _id: { entityID: '$entityID', entitytype: '$entityType' }, rating: { $avg: '$value' } } }]);

    const ratingsMap = {};
    for (let i = 0; i < ratings.length; i += 1) {
      ratingsMap[ratings[i]._id.entityID] = ratings[i].rating;
    }
    const ratedCourses = await Courses.map((e) => e.toObject());
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < ratedCourses.length; i += 1) {
      if (ratingsMap[ratedCourses[i]._id.toString()]) {
        ratedCourses[i].rating = (ratingsMap[ratedCourses[i]._id.toString()]);
      } else {
        ratedCourses[i].rating = 0;
      }
    }

    const numberOfCourses = await Course.countDocuments({ $or: query, publish: 'published' });
    return res.send({ count: numberOfCourses, courses: ratedCourses });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

const editCourse = async (req, res) => {
  try {
    const {
      heading,
      subheading,
      description,
      markedPrice,
      price,
      courseLengthInHrs,
      freeSessions,
      featured,
      language,
      subtitles,
      lifeTimeAccess,
      certificateOfCompletion,
      chapters,
      individualSessions,
      courseValidity,
    } = req.body;

    const course = await Course.findById(req.params.courseid);

    if (course.user.toString() !== req.user.id) {
      return res.status(404).json({ msg: 'Not Authorized' });
    }

    if (!course) {
      return res.status(404).json({ msg: 'Course not found' });
    }
    course.heading = heading;
    course.subheading = subheading;
    course.description = description;
    course.markedPrice = markedPrice;
    course.price = price;
    course.courseLengthInHrs = courseLengthInHrs;
    course.freeSessions = freeSessions;
    course.featured = featured;
    course.language = language;
    course.certificateOfCompletion = certificateOfCompletion;
    course.subtitles = subtitles;
    course.lifeTimeAccess = lifeTimeAccess;
    course.individualSessions = individualSessions;
    course.courseValidity = courseValidity;
    if (req.body.chapters) {
      course.chapters = chapters;
    }

    if (req.body.source) {
      course.source.type = 'link';
      course.source.url = req.body.source;
      course.source.size = 0;
      course.source.fileName = '';
    } else if (req.files[0]) {
      const uploadImage = await uploadToS3(req.files[0].path);
      const url = ((await uploadImage).Location);
      course.source.url = url;
      course.source.size = req.files[0].size;
      course.source.type = req.files[0].mimetype;
      course.source.fileName = req.files[0].originalname;
    }
    await course.save();

    return res.json(course);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server error');
  }
};

const deleteCourse = async (req, res) => {
  try {
    const course = await Course.findById(req.params.courseid);

    if (!course) {
      return res.status(404).json({ msg: 'Course not found' });
    }
    if (course.user.toString() !== req.user.id) {
      return res.status(404).json({ msg: 'User Not Authorized' });
    }
    const user = await User.findById(req.user.id);

    await Course.deleteOne({ id: req.params.id });

    const index = user.coursesCreated.indexOf(req.params.id);
    if (index > -1) {
      user.coursesCreated.splice(index, 1);
    }

    await course.save();
    await user.save();

    return res.json({ msg: 'Course Deleted' });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server error');
  }
};

const getMyCourses = async (req, res) => {
  try {
    const courses = await Enrolled.find({ user: req.user.id }).populate([{ path: 'course' }, { path: 'instructorId', select: 'name' }]);
    return res.json(courses);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getMyCreatedCourses = async (req, res) => {
  try {
    // eslint-disable-next-line max-len
    //  const courses = await Course.find({ user: req.user.id }).populate({ path: 'user', select: 'name', populate: { path: 'profile', select: 'profilePic' } });
    const courses = await Course.aggregate([
      { $match: { user: mongoose.Types.ObjectId(req.user.id) } },
      {
        $lookup: {
          from: 'users', localField: 'user', foreignField: '_id', as: 'userDetails',
        },
      },
      {
        $lookup: {
          from: 'profiles',
          let: { u_id: '$user' },
          pipeline: [

            {
              $match: {
                $expr:
                  { $eq: ['$user', '$$u_id'] },
              },
            },
          ],
          as: 'profileDetails',
        },
      },
      {
        $lookup: {
          from: 'enrolleds',
          let: { c_id: '$_id' },
          pipeline: [

            {
              $match: {
                $expr:
                  { $eq: ['$course', '$$c_id'] },
              },
            },
            { $count: 'total_enrollments' },
          ],
          as: 'enrollmentDetails',
        },
      },
      {
        $lookup: {
          from: 'assignments',
          let: { c_id: '$_id' },
          pipeline: [

            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$courseId', '$$c_id'] },
                    { $eq: ['$status', 0] },
                  ],
                },
              },
            },
            { $count: 'pending_assignments' },
          ],
          as: 'assignDetails',
        },
      },
      {
        $project: {
          _id: 1,
          publish: 1,
          courseDetails: { _id: 1, heading: '$heading' },
          mentorDetails: { _id: '$user', name: '$userDetails.name' },
          studentsCount: { $first: '$enrollmentDetails' },
          pendingAssignments: { $first: '$assignDetails' },
        },
      },
    ]);
    return res.json(courses);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

module.exports = {
  createCourse,
  getCourseById,
  getAllCourses,
  editCourse,
  deleteCourse,
  getMyCourses,
  getMyCreatedCourses,
  // buyCourse,
};
