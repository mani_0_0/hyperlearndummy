const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const dayjs = require('dayjs');
const { config } = require('../config/default');
const { features } = require('../config/features');
const Otp = require('../models/Otp');
const User = require('../models/User');

const getLoggeduser = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);

    const result = { ...features, ...user.toObject() };
    return res.json(result);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('server error');
  }
};

// eslint-disable-next-line consistent-return
const userLogin = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { otp, query } = req.body;
  try {
    const user = await User.findOne(query);

    if (!user) {
      return res.status(400).json({ msg: 'User Does Not Exist' });
    }
    let otpCheck;
    if (query.phone) {
      otpCheck = await Otp.findOne({ generatedFrom: 'phone', source: query.phone }).sort({ updatedAt: -1 }).limit(1);
      if (otpCheck) {
        if (config.environment !== 'production') {
          otpCheck.otp = 111111;
          await otpCheck.save();
        }
      }
    } else {
      otpCheck = await Otp.findOne({ generatedFrom: 'email', source: query.email }).sort({ updatedAt: -1 }).limit(1);
    }

    if (!otpCheck || otpCheck.isUsed === true) {
      return res.status(400).json({ msg: 'OTP Does not exist' });
    }
    const now = dayjs();
    // eslint-disable-next-line max-len
    const twentyMinutesLater = new Date(otpCheck.createdAt.getTime() + (config.OTP_VALIDITY_IN_MINS * 60 * 1000));
    const newdate = new Date(now.format());
    if (newdate > ((twentyMinutesLater))) {
      return res.status(400).json({ msg: 'OTP Expired' });
    }

    if (otp !== otpCheck.otp.toString()) {
      return res.status(400).json({ msg: 'Enter Valid OTP' });
    }

    otpCheck.isUsed = true;
    await otpCheck.save();
    const payload = {
      user: {
        id: user.id,
        user_type: user.user_type,
      },
    };

    jwt.sign(
      payload,
      config.secret,
      { expiresIn: '7d' },
      (err, token) => {
        if (err) throw err;
        res.json({ token });
        // eslint-disable-next-line comma-dangle
      }
    );
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err);
    res.status(500).send('Server Error');
  }
};

module.exports = { getLoggeduser, userLogin };
