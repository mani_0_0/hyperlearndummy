const { validationResult } = require('express-validator');
const oAuthToken = require('../models/oAuthToken');
const { createProfile: createOnboardingProfile } = require('../services/profile.service');
const Profile = require('../models/Profile');
const { createAvailability } = require('../services/availability.service');
const { createSession } = require('../services/session.service');
const { autocompleteCompanySearch } = require('../services/clearbit.service');

const firstSession = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors });
    }
    const resp = await createSession(req.body, req.user.id);
    const profile = await Profile.findOne({ user: req.user.id });
    profile.onBoardingV1.firstSession = true;
    await profile.save();
    return res.status(200).json(resp);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const setAvailability = async (req, res) => {
  try {
    if (!req.body.timeZone) {
      return res.status(400).json({ msg: 'Please Specify Timezone' });
    }
    const token = await oAuthToken.findOne({ userId: req.user.id });
    if (!token) {
      return res.status(400).json({ msg: 'Please Connect Google Cal' });
    }
    const resp = await createAvailability(req.body, req.user.id);
    const profile = await Profile.findOne({ user: req.user.id });
    profile.onBoardingV1.availability = true;
    await profile.save();

    return res.status(200).json(resp);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const createProfile = async (req, res) => {
  try {
    const resp = await createOnboardingProfile(req, req.user.id);
    const profile = await Profile.findOne({ user: req.user.id });
    profile.onBoardingV1.profile = true;
    await profile.save();

    return res.json(resp);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const skipOnboarding = async (req, res) => {
  try {
    const profile = await Profile.findOne({ user: req.user.id });
    profile.onBoardingV1.skip = true;
    await profile.save();
    return res.status(200).json({ msg: 'Okay! We wont bug you again.' });
  } catch (error) {
  // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const companyAutoSearch = async (req, res) => {
  try {
    const { name } = req.query;
    const resp = await autocompleteCompanySearch(name);

    return res.status(200).json({ companies: resp.data });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err.message);
    return res.status(500).send('Server Error');
  }
};

module.exports = {
  firstSession,
  setAvailability,
  createProfile,
  skipOnboarding,
  companyAutoSearch,
};
