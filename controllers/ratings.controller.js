const { validationResult } = require('express-validator');
const Rating = require('../models/Rating');

const rateEntity = async (req, res) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { entityType, entityID } = req.params;

    const { value } = req.body;
    // eslint-disable-next-line max-len
    const rating = await Rating.findOne({ user: req.user.id, entityType, entityID });

    if (rating) {
      rating.value = value;
      await rating.save();
      return res.status(200).json(rating);
    }
    const newRating = new Rating({
      user: req.user.id,
      entityType,
      entityID,
      value,
    });

    await newRating.save();
    return res.status(200).json(newRating);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getRating = async (req, res) => {
  try {
    const { entityID, entityType } = req.params;
    const rating = await Rating.findOne({ user: req.user.id, entityID, entityType });
    return res.status(200).json(rating);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};
module.exports = {
  rateEntity,
  getRating,
};
