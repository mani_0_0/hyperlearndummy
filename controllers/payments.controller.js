const crypto = require('crypto');
const mongoose = require('mongoose');
const Promise = require('bluebird');
const _ = require('lodash');
const { config } = require('../config/default');
const { razorpayPayment } = require('../services/payment.service');
const Order = require('../models/Order');
const Payment = require('../models/Payment');
const paymentService = require('../models/paymentService');
const { createEnrollment } = require('../utils/createEnrollment');
const { createBooking } = require('../utils/createBooking');

const verifyPayment = async (req, res) => {
  try {
    const secret = config.razorpayWebhookSecret;

    const shasum = crypto.createHmac('sha256', secret);
    shasum.update(JSON.stringify(req.body));
    const digest = shasum.digest('hex');

    if (digest === req.headers['x-razorpay-signature']) {
      return res.status(200).json({ status: 'ok' });
    }
    // pass it
    return res.status(400).json({ status: 'invalid' });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const updateOrder = async (req, res) => {
  try {
    const payment = await Payment.findById(req.body.paymentId);
    if (!payment) {
      return res.status(404).json({ msg: 'Order not found' });
    }
    payment.status = req.body.status;
    payment.additionalDetails = req.body.details;

    await payment.save();

    if (payment.status === 'completed') {
      const order = await Order.findById(req.body.orderId);
      if (!order) {
        return res.status(404).json({ msg: 'Order Not found' });
      }

      if (order.entityType === 'course') {
        const enrollment = await createEnrollment(order.user, order.entityId);
        return res.status(200).json(enrollment);
      }
      if (order.entityType === 'session_booking') {
        const booking = await createBooking(req.body.info, order.user);
        return res.status(200).json(booking);
      }
      return res.status(400).json({ msg: 'Invalid request' });
    }

    return res.status(400).json({ msg: 'Invalid request' });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const makePayment = async (req, res) => {
  try {
    const {
      entityId, entityType, price, total, payeeId, buyerId, paymentMode, amount,
    } = req.body;

    const neworder = new Order({
      user: req.user.id,
      entityId,
      entityType,
      price,
      total,
    });

    const newPayment = new Payment({
      payeeId, buyerId, paymentMode, amount: amount / 100,
    });
    let details = {};
    if (amount > 0) {
      if (paymentMode === 'razorpay') {
        details = await razorpayPayment(amount);
      }
    } else if (amount === 0) {
      newPayment.status = 'completed';
    }
    newPayment.paymentModeId = details.id;
    newPayment.receipt = details.receipt;
    await newPayment.save();
    neworder.paymentId = newPayment.id;
    await neworder.save();
    return res.status(200).json({ newPayment, details, neworder });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getOrders = async (req, res) => {
  try {
    const pipeline = [
      {
        $match: { user: mongoose.Types.ObjectId(req.user.id) },
      },
      {
        $lookup: {
          from: 'course',
          let: { c_id: '$entityId' },
          pipeline: [
            {
              $match: {
                $expr: { $eq: ['$_id', '$$c_id'] },
              },
            },
          ],
          as: 'courseDetails',
        },
      },
      {
        $project: {
          id: 1,
          paymentId: 1,
          price: 1,
          total: 1,
          course: { $first: '$courseDetails' },
        },
      },
      {
        $lookup: {
          from: 'users',
          let: { u_id: '$course.user' },
          pipeline: [
            {
              $match: {
                $expr: { $eq: ['$_id', '$$u_id'] },
              },
            },
          ],
          as: 'userDetails',
        },
      },
      {
        $lookup: {
          from: 'payments', localField: 'paymentId', foreignField: '_id', as: 'paymentDetails',
        },
      },
    ];

    const orders = await Order.aggregate(pipeline);
    return res.status(200).json(orders);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const myEarnings = async (req, res) => {
  try {
    const earning = {};
    const date = new Date();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const monthlyEarning = Payment.aggregate([
      {
        $match: { $and: [{ payeeId: mongoose.Types.ObjectId(req.user.id) }, { status: 'completed' }] },
      },
      {
        $group: {
          _id: {
            user: '$payeeId',
            month: {
              $month: '$createdAt',
            },
            year: { $year: '$createdAt' },
          },

          earnings: { $sum: '$amount' },
        },
      },
      {
        $project: {
          user: '$user',
          month: '$_id.month',
          year: '$_id.year',
          earnings: '$earnings',
        },
      },
      {
        $match: { $and: [{ month }, { year }] },
      },
    ]);
    const totalEarning = Payment.aggregate([
      {
        $match: { $and: [{ payeeId: mongoose.Types.ObjectId(req.user.id) }, { status: 'completed' }] },
      },
      {
        $group: {
          _id: {
            user: '$payeeId',
          },

          earnings: { $sum: '$amount' },
        },
      },
      {
        $project: {
          user: '$user',
          earnings: '$earnings',
        },
      },
    ]);

    const values = await Promise.all([totalEarning, monthlyEarning]);
    if (values[0]) {
      earning.totalEarning = _.get(values[0][0], 'earnings', 0);
    } else {
      earning.totalEarning = 0;
    }

    if (values[1]) {
      earning.monthlyEarning = _.get(values[1][0], 'earnings', 0);
    } else {
      earning.monthlyEarning = 0;
    }
    return res.status(200).json(earning);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getServices = async (req, res) => {
  try {
    const services = await paymentService.find({ isActive: true });
    return res.status(200).json(services);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};
module.exports = {
  verifyPayment,
  updateOrder,
  getOrders,
  makePayment,
  myEarnings,
  getServices,
};
