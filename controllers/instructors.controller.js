const mongoose = require('mongoose');
const Course = require('../models/Course');
const Instructor = require('../models/Instructor');
const User = require('../models/User');

const sendInvite = async (req, res) => {
  try {
    const { user } = req.body;
    const course = req.params.courseid;

    // eslint-disable-next-line max-len
    const instructorCheck = await Instructor.findOne({ $or: [{ user, course, status: 'pending' }, { user, course, status: 'accepted' }] });

    if (instructorCheck) {
      return res.status(400).json({ msg: 'User Already sent invite' });
    }
    const instructor = new Instructor({
      user,
      course,
    });
    const Courses = await Course.findById(course);

    if (!Courses) {
      return res.status(400).json({ msg: 'Course not found' });
    }

    await instructor.save();
    return res.status(200).json(instructor);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const inviteToCourse = async (req, res) => {
  try {
    const verify = { accepted: 'accepted', rejected: 'rejected' };

    const { status } = req.body;

    const { instructorId } = req.params;
    const request = await Instructor.findById(instructorId);
    if (!request) {
      return res.status(404).json({ msg: 'Invite not found' });
    }
    if (verify[status]) {
      request.status = status;
      await request.save();

      return res.status(200).json(request);
    }

    return res.status(400).json({ msg: 'Invalid Request' });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const showInvites = async (req, res) => {
  try {
    const match = { user: mongoose.Types.ObjectId(req.user.id) };
    if (req.query.status) {
      const { status } = req.query;
      match.status = status;
    }

    const pipeline = [
      { $match: match },
      {
        $lookup: {
          from: 'course', localField: 'course', foreignField: '_id', as: 'courseDetails',
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'courseDetails.user',
          foreignField: '_id',
          as: 'mentorDetails',
        },
      },
      {
        $lookup: {
          from: 'enrolleds',
          let: { c_id: '$course' },
          pipeline: [
            {
              $match: {
                $expr:
                  { $eq: ['$course', '$$c_id'] },
              },
            },
            { $count: 'total_students' },
          ],
          as: 'enrolledStudent',
        },
      },
      {
        $project: {
          _id: 1,
          user: 1,
          course: 1,
          status: 1,
          mentor: { $first: '$courseDetails.user' },
          mentorName: { $first: '$mentorDetails.name' },
          courseName: { $first: '$courseDetails.heading' },
          totalStudents: { $first: '$enrolledStudent' },
        },
      },
    ];
    const instructorRequests = await Instructor.aggregate(pipeline);
    return res.status(200).json(instructorRequests);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getInstructors = async (req, res) => {
  try {
    const courseIds = await Course.find({ user: req.user.id }).select('id');
    const ids = await courseIds.map((e) => mongoose.Types.ObjectId(e.id));
    const match = { $expr: { $in: ['$course', ids] } };
    if (req.query.status) {
      match.status = req.query.status;
    }
    if (req.query.courseId) {
      match.course = mongoose.Types.ObjectId(req.query.courseId);
    }

    const pipeline = [{ $match: match },
      {
        $lookup: {
          from: 'course', localField: 'course', foreignField: '_id', as: 'courseDetails',
        },
      },
      {
        $lookup: {
          from: 'users', localField: 'user', foreignField: '_id', as: 'instructorDetails',
        },
      },
      {
        $lookup: {
          from: 'profiles',
          let: { u_id: '$user' },
          pipeline: [

            {
              $match: {
                $expr:
                { $eq: ['$user', '$$u_id'] },
              },
            },
          ],
          as: 'profileDetails',
        },
      },
      {
        $project: {
          _id: 1,
          user: 1,
          course: 1,
          status: 1,
          mentor: { $first: '$courseDetails.user' },
          courseName: { $first: '$courseDetails.heading' },
          instructorName: { $first: '$instructorDetails.name' },
          email: { $first: '$instructorDetails.email' },
          phone: { $first: '$instructorDetails.phone' },
          instructorProfile: { $first: '$profileDetails' },
        },
      },
    ];

    if (req.query.search) {
      const search = [];
      const rex = new RegExp(req.query.search);
      search.push({ courseName: { $regex: rex, $options: 'i' } });
      search.push({ instructorName: { $regex: rex, $options: 'i' } });
      pipeline.push({ $match: { $or: search } });
    }

    let offset = 0;
    if (req.query.offset) {
      offset = Number(req.query.offset);
    }
    let limit = 10;
    if (req.query.limit) {
      limit = Number(req.query.limit);
    }

    pipeline.push({
      $facet: {
        paginatedResults: [{ $skip: offset }, { $limit: limit }],
        totalCount: [
          {
            $count: 'count',
          },
        ],
      },
    });
    const instructors = await Instructor.aggregate(pipeline);
    return res.status(200).json(instructors);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getSharedCourses = async (req, res) => {
  try {
    const pipeline = [
      { $match: { user: mongoose.Types.ObjectId(req.user.id) } },
      {
        $lookup: {
          from: 'course', localField: 'course', foreignField: '_id', as: 'courseDetails',
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'courseDetails.user',
          foreignField: '_id',
          as: 'mentorDetails',
        },
      },
      {
        $lookup: {
          from: 'enrolleds',
          let: { c_id: '$course', u_id: '$user' },
          pipeline: [
            {
              $match: {
                $expr:
                {
                  $and: [
                    { $eq: ['$course', '$$c_id'] },
                    { $eq: ['$instructorId', '$$u_id'] },
                  ],
                },
              },
            },
            { $count: 'total_students' },
          ],
          as: 'enrolledStudent',
        },
      },
      {
        $project: {
          _id: 1,
          user: 1,
          course: 1,
          status: 1,
          mentor: { $first: '$courseDetails.user' },
          mentorName: { $first: '$mentorDetails.name' },
          courseName: { $first: '$courseDetails.heading' },
          totalStudents: { $first: '$enrolledStudent' },
        },
      },
    ];
    const courses = await Instructor.aggregate(pipeline);
    return res.status(200).json(courses);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const searchInstructor = async (req, res) => {
  try {
    const query = [];
    if (req.query.search) {
      const rex = new RegExp(req.query.search);
      query.push({ name: { $regex: rex, $options: 'i' } });
      query.push({ email: { $regex: rex, $options: 'i' } });
      query.push({ phone: { $regex: rex, $options: 'i' } });
    } else {
      query.push({});
    }
    const users = await User.find({ user_type: 'mentor', $or: query }).populate({ path: 'profile' });

    return res.status(200).json(users);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};
module.exports = {
  sendInvite,
  showInvites,
  inviteToCourse,
  getInstructors,
  getSharedCourses,
  searchInstructor,
};
