const Course = require('../models/Course');
const Request = require('../models/Request');

const makeRequest = async (req, res) => {
  try {
    const { entityId, entityType } = req.body;

    const course = await Course.findById(entityId);
    if (course.publish === 'pending') {
      return res.json({ msg: 'Request Already Sent' });
    } if (course.publish === 'published' && entityType === 'publishCourse') {
      return res.json({ msg: 'Course Already published' });
    } if (course.publish === 'unpublished' && entityType === 'unpublishCourse') {
      return res.json({ msg: 'Course Already unpublished' });
    }
    const newRequest = new Request({
      requestedBy: req.user.id,
      entityId,
      entityType,
    });

    course.publish = 'pending';
    await newRequest.save();
    return res.json(newRequest);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const reviewRequest = async (req, res) => {
  try {
    const { comments, status, requestId } = req.body;
    const request = await Request.findById(requestId);
    if (!request) {
      return res.json({ msg: 'Request Not found' });
    }
    request.status = status;
    request.comments = comments;
    request.reviewedBy = req.user.id;

    if (status === 'accepted') {
      const course = await Course.findById(request.entityId);
      if (!course) {
        return res.status(404).json({ msg: 'Course not found' });
      }
      if (request.entityType === 'unpublishCourse') {
        course.publish = 'unpublished';
      } else {
        course.publish = 'published';
      }

      await course.save();
    }

    await request.save();
    return res.status(200).json(request);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getRequests = async (req, res) => {
  try {
    const query = {};
    if (req.query.userId) {
      query.requestedBy = req.query.userId;
    }
    if (req.query.status) {
      query.status = req.query.status;
    }

    const requests = await Request.find(query);

    return res.status(200).json(requests);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

module.exports = { getRequests, reviewRequest, makeRequest };
