/* eslint-disable max-len */
const _ = require('lodash');
const Assignment = require('../models/Assignment');
const Message = require('../models/Message');
const Module = require('../models/Module');
const Enrolled = require('../models/Enrolled');
const User = require('../models/User');

const viewAssignments = async (req, res) => {
  try {
    const { enrollmentId } = req.params;
    const enrollment = await Enrolled.findById(enrollmentId).select('course').populate([{
      path: 'course',
      select: 'heading',
      populate: {
        path: 'chapters',
        populate: { path: 'modules', match: { moduleType: 'assignment' } },
      },
    }]);
    if (!enrollment) {
      return res.status(404).json({ msg: 'Enrollment not found' });
    }
    const assignments = await Assignment.find({ enrollmentId });

    const assignmentMap = {};
    for (let i = 0; i < assignments.length; i += 1) {
      assignmentMap[assignments[i].moduleId] = assignments[i];
    }
    const courseAssignment = enrollment.toObject();
    for (let i = 0; i < courseAssignment.course.chapters.length; i += 1) {
      for (let j = 0; j < courseAssignment.course.chapters[i].modules.length; j += 1) {
        // eslint-disable-next-line no-underscore-dangle
        if (assignmentMap[courseAssignment.course.chapters[i].modules[j]._id]) {
          // eslint-disable-next-line no-underscore-dangle
          courseAssignment.course.chapters[i].modules[j].submission = assignmentMap[courseAssignment.course.chapters[i].modules[j]._id];
        } else {
          courseAssignment.course.chapters[i].modules[j].submission = 'Not Submitted';
        }
      }
    }
    return res.json(courseAssignment);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const submitAssignment = async (req, res) => {
  try {
    const {
      courseId, moduleId, enrollmentId, source,
    } = req.body;

    const enrollment = await Enrolled.findById(enrollmentId).populate([{ path: 'user', select: 'name' }, { path: 'course' }]);
    if (!enrollment) {
      return res.status(404).json({ msg: 'Enrollment not found' });
    }
    const assignment = await Assignment.findOne({ courseId, moduleId, enrollmentId });
    if (assignment) {
      assignment.source.url = source;
      assignment.status = 0;
      assignment.reviewedBy = null;
      await assignment.save();
      const newMessage = new Message({
        senderId: enrollment.course.user.toString(),
        receiverId: req.user.id,
        courseId,
        msgType: 'system',
        enrollmentId,
      });

      const module = await Module.findById(moduleId);
      newMessage.source.msg = `${enrollment.user.name} has submitted assignment in ${module.heading}`;
      await newMessage.save();
      return res.status(200).json(assignment);
    }
    const newAssignment = new Assignment({
      courseId, moduleId, enrollmentId, status: 0,
    });

    newAssignment.source.url = source;
    newAssignment.source.type = 'link';
    newAssignment.reviewedBy = null;
    await newAssignment.save();

    const newMessage = new Message({
      senderId: enrollment.course.user.toString(),
      receiverId: req.user.id,
      courseId,
      msgType: 'system',
      enrollmentId,
    });

    const module = await Module.findById(moduleId);
    newMessage.source.msg = `${enrollment.user.name} has submitted assignment in ${module.heading}`;
    await newMessage.save();

    return res.status(200).json(newAssignment);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const reviewAssignments = async (req, res) => {
  try {
    const assignment = await Assignment.findById(req.params.assignmentId).populate([
      { path: 'enrollmentId', populate: { path: 'user', select: 'name' } },
      { path: 'moduleId' },
      { path: 'courseId' },
    ]);

    if (!assignment) {
      return res.status(404).json({ msg: 'Assignment not found' });
    }

    assignment.score = req.body.score;
    assignment.comments = req.body.comments;
    assignment.status = 1;
    assignment.reviewedBy = req.user.id;
    await assignment.save();

    const newMessage = new Message({
      senderId: _.get(assignment, 'courseId.user', null),
      receiverId: _.get(assignment, 'enrollmentId.user.id', null),
      courseId: _.get(assignment, 'courseId.id', null),
      msgType: 'system',
      enrollmentId: _.get(assignment, 'enrollmentId.id', null),
    });
    const reviewBy = await User.findById(req.user.id).select('name');
    newMessage.source.msg = `${_.get(reviewBy, 'name')} has reveiwed assignment in ${_.get(assignment, 'moduleId.heading')}`;
    await newMessage.save();
    return res.status(200).json(assignment);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getModuleAssignments = async (req, res) => {
  try {
    const { moduleId, enrollmentId } = req.params;
    const assignments = await Assignment.find({ moduleId, enrollmentId });
    return res.status(200).json(assignments);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

module.exports = {
  viewAssignments, getModuleAssignments, reviewAssignments, submitAssignment,
};
