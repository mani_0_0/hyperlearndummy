const { validationResult } = require('express-validator');

const Course = require('../models/Course');
const Chapter = require('../models/Chapter');

const createChapter = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const course = await Course.findById(req.params.courseid);
    if (!course) {
      return res.status(404).json({ msg: 'course not found' });
    }

    if (course.user.toString() !== req.user.id) {
      return res.status(400).json({ msg: 'User not authorized' });
    }

    const {
      heading, description, time, locked,
    } = req.body;
    const newChapter = new Chapter({
      course: req.params.courseid,
      heading,
      description,
      time,
      locked,
    });

    await newChapter.save();

    course.chapters.push(newChapter.id);

    await course.save();

    return res.status(200).json(newChapter);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

const editChapter = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const {
      heading, description, locked, time, modules,
    } = req.body;
    const chapter = await Chapter.find({ _id: req.params.chapterid, course: req.params.courseid }).populate('course');

    if (!chapter[0]) {
      return res.status(404).json({ msg: 'Chapter not found' });
    }

    if (chapter[0].course.user.toString() !== req.user.id) {
      return res.status(400).json({ msg: 'User not authorized' });
    }

    chapter[0].heading = heading;
    chapter[0].description = description;
    chapter[0].locked = locked;
    chapter[0].time = time;
    if (req.body.modules) {
      chapter[0].modules = modules;
    }

    await chapter[0].save();

    return res.status(200).json(chapter[0]);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

const deleteChapter = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const chapter = await Chapter.findById(req.params.chapterid);

    if (!chapter) {
      return res.status(404).json({ msg: 'Chapter not found' });
    }

    const course = await Course.findById(req.params.courseid);

    if (!course) {
      return res.status(404).json({ msg: 'Course not found' });
    }

    if (course.user.toString() !== req.user.id) {
      return res.status(400).json({ msg: 'User not authorized' });
    }

    await Chapter.deleteOne({ id: req.params.chapterid });
    const index = course.chapters.indexOf(req.params.chapterid);
    if (index > -1) {
      course.chapters.splice(index, 1);
    }

    await course.save();

    return res.status(200).json({ msg: 'Chapter Deleted' });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

module.exports = { createChapter, editChapter, deleteChapter };
