/* eslint-disable no-underscore-dangle */
const DateTime = require('node-datetime');
const { google } = require('googleapis');
const Promise = require('bluebird');
const { validationResult } = require('express-validator');

const { OAuth2 } = google.auth;
const { config } = require('../config/default');
const Availability = require('../models/Availability');
const Booking = require('../models/Booking');
const oAuthToken = require('../models/oAuthToken');
const Session = require('../models/Session');
const { createAvailability } = require('../services/availability.service');
const { createSession: createSession_ } = require('../services/session.service');

const oAuth2Client = new OAuth2(
  config.googleClientId,
  config.googleClientSecret,
  config.redirectUri,
);

const getAvailability = async (req, res) => {
  try {
    const availability = await Availability.find({ userId: req.params.mentorId });
    if (!availability) {
      return res.status(404).json({ msg: 'Not found' });
    }

    return res.status(200).json(availability);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const getBookings = async (req, res) => {
  try {
    const { from, to } = req.body;
    const { userId } = req.params;
    const fromDate = DateTime.create(from);
    const toDate = DateTime.create(to);

    if (req.query.type === 'student') {
      const booking = await Booking.find(
        { userId, startTime: { $gte: fromDate._now, $lte: toDate._now } },
      );
      const result = {};
      result.googlecal = {};
      result.scheduler = booking;
      return res.status(200).json(result);
    }

    const booking = Booking.find(
      { mentorId: userId, startTime: { $gte: fromDate._now, $lte: toDate._now } },
    );
    const auth = await oAuthToken.findOne({ userId });

    if (!auth) {
      return res.status(400).json({ msg: 'Please connect calender' });
    }
    oAuth2Client.setCredentials(auth.oAuthtoken);
    const calendar = google.calendar({ version: 'v3', auth: oAuth2Client });

    const getEvents = calendar.events.list({
      calendarId: 'primary',
      timeMin: fromDate._now.toISOString(),
      timeMax: toDate._now.toISOString(),
      singleEvents: true,
      orderBy: 'startTime',
    });
    const temp = await Promise.all([getEvents, booking]);
    const result = {};
    if (temp[0]) {
      const filtered = [];
      for (let i = 0; i < temp[0].data.items.length; i += 1) {
        filtered.push({
          startTime: temp[0].data.items[i].start.dateTime,
          endTime: temp[0].data.items[i].end.dateTime,
        });
      }
      result.googleCal = filtered;
    }

    if (temp[1]) {
      // eslint-disable-next-line prefer-destructuring
      result.scheduler = temp[1];
    }
    return res.status(200).json(result);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const makeBooking = async (req, res) => {
  try {
    const {
      mentorId, startTime, endTime, status, orderId, attendees, comments,
    } = req.body;
    const formatStartTime = DateTime.create(startTime);
    const formatEndTime = DateTime.create(endTime);

    const newBooking = new Booking({
      mentorId,
      userId: req.user.id,
      startTime: formatStartTime._now,
      endTime: formatEndTime._now,
      status,
      orderId,
      comments,
    });
    const auth = await oAuthToken.findOne({ userId: mentorId });

    if (!auth) {
      return res.status(400).json({ msg: 'Please connect calender' });
    }
    oAuth2Client.setCredentials(auth.oAuthtoken);
    const calendar = google.calendar({ version: 'v3', auth: oAuth2Client });

    const session = await Session.findById(orderId.entityId);

    await newBooking.save();

    const bookingMade = calendar.events.insert({
      calendarId: 'primary',
      resource: {
        summary: session.name,
        description: session.description,
        start: {
          dateTime: formatStartTime._now.toISOString(),
          timeZone: 'utc',
        },
        end: {
          dateTime: formatEndTime._now.toISOString(),
          timeZone: 'utc',
        },
        attendees: [attendees],
        reminders: {
          useDefault: true,
          overrides: [
            { method: 'email', minutes: 24 * 60 },
            { method: 'popup', minutes: 10 },
          ],
        },
        sendNotifications: true,
        conferenceDataVersion: 1,
        conferenceData: {
          createRequest: {
            conferenceSolutionKey: {
              type: 'hangoutsMeet',
            },
            requestId: newBooking.id.toString(),
          },
        },
        sendUpdates: 'all',
        colorId: 4,
        status: 'confirmed',
      },
    });

    const temp = await Promise.all([bookingMade]);

    const result = {};
    if (temp[0]) {
      // eslint-disable-next-line prefer-destructuring
      result.googleBooking = temp[0];
    }

    result.schedulerBooking = newBooking;

    return res.status(200).json(result);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const setAvailability = async (req, res) => {
  try {
    const resp = await createAvailability(req.body, req.user.id);
    return res.status(200).json(resp);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};
const googleOAuthLoginLink = async (req, res) => {
  try {
    const scope = 'https://www.googleapis.com/auth/calendar';
    const url = oAuth2Client.generateAuthUrl({
      // 'online' (default) or 'offline' (gets refresh_token)
      access_type: 'offline',
      // If you only need one scope you can pass it as a string
      prompt: 'consent',
      scope,
      state: req.user.id,
    });
    return res.json(url);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const checkToken = async (req, res) => {
  try {
    const { mentorId } = req.params;
    const token = await oAuthToken.findOne({ userId: mentorId });
    if (!token) {
      return res.status(404).json({ msg: 'No token exists' });
    }
    return res.status(200).json({ msg: 'Token exists' });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const createSession = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors });
    }
    const resp = await createSession_(req.body, req.user.id);
    return res.status(200).json(resp);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

const getSessions = async (req, res) => {
  try {
    if (req.query.sessionId) {
      const sessions = await Session.findById(req.query.sessionId);
      return res.status(200).json(sessions);
    }
    if (req.query.mentorId) {
      const sessions = await Session.find({ mentorId: req.query.mentorId });
      return res.status(200).json(sessions);
    }

    return res.status(400).send('Bad Request');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return res.send('Server Error');
  }
};

module.exports = {
  getBookings,
  getAvailability,
  makeBooking,
  setAvailability,
  googleOAuthLoginLink,
  checkToken,
  createSession,
  getSessions,
};
