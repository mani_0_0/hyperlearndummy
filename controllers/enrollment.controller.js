const Enrolled = require('../models/Enrolled');

const { createEnrollment } = require('../utils/createEnrollment');

const newEnrollment = async (req, res) => {
  try {
    const { course } = req.body;
    const result = await createEnrollment(req.user.id, course);
    return res.status(200).json(result);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const moduleWatched = async (req, res) => {
  // eslint-disable-next-line no-console
  try {
    const enrollment = await Enrolled.findById(req.params.enrollmentid);
    const { module } = req.body;
    if (!enrollment) {
      return res.status(404).json({ msg: 'No Enrollment found' });
    }
    const index = await enrollment.modules.findIndex((x) => x.id.toString() === module);
    if (index === -1) {
      enrollment.modules.push(module);
      enrollment.modules[enrollment.modules.length - 1].isWatched = true;
      await enrollment.save();
      return res.status(200).json(enrollment);
    }
    enrollment.modules[index].isWatched = !enrollment.modules[index].isWatched;

    await enrollment.save();
    return res.status(200).json(enrollment);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const moduleSaved = async (req, res) => {
  // eslint-disable-next-line no-console
  try {
    const enrollment = await Enrolled.findById(req.params.enrollmentid);
    const { module } = req.body;
    if (!enrollment) {
      return res.status(404).json({ msg: 'No Enrollment found' });
    }

    const index = await enrollment.modules.findIndex((x) => x.id.toString() === module);
    if (index === -1) {
      enrollment.modules.push(module);
      enrollment.modules[enrollment.modules.length - 1].isSaved = true;
      await enrollment.save();
      return res.status(200).json(enrollment);
    }
    enrollment.modules[index].isSaved = !enrollment.modules[index].isSaved;
    await enrollment.save();
    return res.status(200).send(enrollment);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

// const unenroll = async (req, res) => {
//   try {
//     const { user, course } = req.body;

//     await Enrolled.findOneAndDelete({ user, course });

//     return res.status(200).json({ msg: 'User Unenrolled' });
//   } catch (error) {
//     // eslint-disable-next-line no-console
//     console.log(error.message);
//     return res.status(500).send('Server Error');
//   }
// };

// to check if a student is enrolled in course
const getEnrollment = async (req, res) => {
  try {
    if (req.query.courseid) {
      const { courseid } = req.query;
      const enrollment = await Enrolled.find({ user: req.user.id, course: courseid }).populate('course');

      if (!enrollment[0]) {
        return res.status(404).json({ msg: 'Student not enrolled' });
      }
      return res.status(200).json(enrollment);
    } if (req.query.enrollmentid) {
      const { enrollmentid } = req.query;
      const enrollment = await Enrolled.find({ _id: enrollmentid }).populate('course');

      if (!enrollment[0]) {
        return res.status(404).json({ msg: 'Wrong Enrollment ID' });
      }
      return res.status(200).json(enrollment);
    }
    const enrollment = await Enrolled.find({ user: req.user.id }).populate({ path: 'course', select: 'heading id' });
    if (!enrollment[0]) {
      return res.status(404).json({ msg: 'No courses Found' });
    }
    return res.status(200).json(enrollment);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

module.exports = {
  newEnrollment, moduleWatched, moduleSaved, getEnrollment,
};
