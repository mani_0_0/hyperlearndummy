const { validationResult } = require('express-validator');

const Chapter = require('../models/Chapter');
const Module = require('../models/Module');
const Course = require('../models/Course');
const { uploadToS3 } = require('../services/uploadToS3.service');

const createModule = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    const chapter = await Chapter.findById(req.params.chapterid).populate('course');

    if (!chapter) {
      return res.status(404).json({ msg: 'Chapter not found' });
    }

    if (chapter.course.user.toString() !== req.user.id) {
      return res.status(400).json({ msg: 'User not authorized' });
    }
    const newModule = new Module({
      chapter: req.params.chapterid,
      heading: req.body.heading,
      description: req.body.description,
      moduleType: req.body.moduleType,
      time: req.body.time,
      courseId: req.params.courseid,
    });
    if (req.body.source) {
      newModule.source.type = 'link';
      newModule.source.url = req.body.source;
      newModule.source.size = 0;
    } else if (req.files[0]) {
      const uploadImage = await uploadToS3(req.files[0].path);
      const url = ((await uploadImage).Location);
      newModule.source.url = url;
      newModule.source.size = req.files[0].size;
      newModule.source.type = req.files[0].mimetype;
      newModule.source.fileName = req.files[0].originalname;
    }
    await newModule.save();
    chapter.modules.push(newModule.id);
    chapter.save();

    return res.status(200).json(newModule);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

const deleteModule = async (req, res) => {
  try {
    // eslint-disable-next-line max-len
    const moduleToDelete = await Module.find({ _id: req.params.moduleid, chapter: req.params.chapterid });

    if (!moduleToDelete[0]) {
      return res.status(404).json({ msg: 'Module not found' });
    }

    const chapter = await Chapter.findById(req.params.chapterid).populate({ path: 'course', select: 'user' });

    if (!chapter) {
      return res.status(404).json({ msg: 'Chapter not found' });
    }

    if (chapter.course.user.toString() !== req.user.id) {
      return res.status(400).json({ msg: 'User not authorized' });
    }

    await Module.deleteOne({ _id: req.params.moduleid });

    const index = chapter.modules.indexOf(req.params.moduleid);
    if (index > -1) {
      chapter.modules.splice(index, 1);
    }

    await chapter.save();

    return res.status(200).json({ msg: 'Module Deleted ' });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

const editModule = async (req, res) => {
  try {
    const module = await Module.find({ _id: req.params.moduleid, chapter: req.params.chapterid });

    if (!module[0]) {
      return res.status(404).json({ msg: 'Module not found' });
    }

    const course = await Course.findById(req.params.courseid);
    if (!course) {
      return res.status(404).json({ msg: 'Invalid Request' });
    }

    if (course.user.toString() !== req.user.id) {
      return res.status(400).json({ msg: 'User not authorized' });
    }

    module[0].heading = req.body.heading;
    module[0].description = req.body.description;
    module[0].moduleType = req.body.moduleType;
    module[0].time = req.body.time;

    if (req.body.source) {
      module[0].source.type = 'link';
      module[0].source.url = req.body.source;
      module[0].source.size = 0;
    } else if (req.files[0]) {
      const uploadImage = await uploadToS3(req.files[0].path);
      const url = ((await uploadImage).Location);
      module[0].source.url = url;
      module[0].source.size = req.files[0].size;
      module[0].source.type = req.files[0].mimetype;
      module[0].source.fileName = req.files[0].originalname;
    }

    await module[0].save();

    return res.status(200).json(module[0]);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};
module.exports = { createModule, deleteModule, editModule };
