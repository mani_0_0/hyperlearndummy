const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const dayjs = require('dayjs');
const mongoose = require('mongoose');
const { config } = require('../config/default');
const Otp = require('../models/Otp');
const User = require('../models/User');
const { sendEmailVerificationLink, sendOtpToUserByEmail } = require('../services/user.service');
const { uploadToS3 } = require('../services/uploadToS3.service');
const { createProfile } = require('../services/profile.service');
const Profile = require('../models/Profile');
const Rating = require('../models/Rating');
const { sendOtpByPhone } = require('../services/user.service');

const verifyUser = async (req, res) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors });
    }

    const { phone, otp } = req.body;
    let user = await User.findOne({ phone });

    if (user) {
      return res.status(400).json({ msg: 'User Already exits' });
    }
    const otpCheck = await Otp.findOne({ generatedFrom: 'phone', source: phone }).sort({ createdAt: -1 }).limit(1);
    if (!otpCheck || otpCheck.isUsed) {
      return res.status(400).json({ msg: 'OTP Does not exist' });
    }
    const now = dayjs();

    if (now.format() > ((otpCheck.createdAt.getMinutes() + config.OTP_VALIDITY_IN_MINS))) {
      return res.status(400).json({ msg: 'OTP Expired' });
    }

    if (otp !== otpCheck.otp.toString()) {
      return res.status(400).json({ msg: 'Enter Valid OTP' });
    }
    user = new User({
      phone,
    });

    await user.save();
    return res.json(user);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

// eslint-disable-next-line consistent-return
const userSignup = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, email, phone } = req.body;

    let user = await User.findOne({ email });

    // If User already Exists
    if (user) {
      return res.status(400).json({ msg: 'User Already exits' });
    }

    user = await User.findOne({ phone });

    if (!user) {
      return res.status(404).json({ msg: 'User not found' });
    }
    // Save User
    user.email = email;
    user.name = name;
    await user.save();

    const payload = {
      user: {
        id: user.id,
        user_type: user.user_type,
      },
    };

    sendEmailVerificationLink(email, user.id);

    jwt.sign(
      payload,
      config.secret,
      { expiresIn: '7d' },
      (err, token) => {
        if (err) throw err;
        return res.json({ token });
      },
    );
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    res.status(500).send('Server Error');
  }
};

const sendEmailVerification = async (req, res) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const user = await User.findById(req.user.id);

    if (!user) {
      res.status(404).json({ msg: 'User Does not Exist' });
    }
    sendEmailVerificationLink(user.enail, req.user.id);
    return res.status(200).send('<h1>Email Sent</h1>');
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err.message);
    return res.status(500).send('Server Error');
  }
};

const sendEmailOtp = async (req, res) => {
  try {
    const { email } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ msg: 'user not found' });
    }
    const otpCheck = await Otp.findOne({ generatedFrom: 'email', source: email }).sort({ updatedAt: -1 }).limit(1);
    if (otpCheck && otpCheck.isUsed === false) {
      const now = dayjs();
      // eslint-disable-next-line max-len
      const twentyMinutesLater = new Date(otpCheck.createdAt.getTime() + (config.OTP_VALIDITY_IN_MINS * 60 * 1000));
      const newdate = new Date(now.format());
      if (newdate < twentyMinutesLater) {
        if (otpCheck.retryAttempts >= config.MAX_RETRY_ATTEMPT) {
          return res.status(400).json({ msg: 'Max resend limit reached. Pls wait for some time' });
        }
        await sendOtpToUserByEmail(email, otpCheck.otp);

        otpCheck.retryAttempts += 1;
        await otpCheck.save();
        return res.status(200).json({ msg: 'OTP sent Again' });
      }
    }
    const otp = Math.floor(100000 + Math.random() * 900000);
    await sendOtpToUserByEmail(email, otp);
    const newotp = new Otp({
      otp, generatedFrom: 'email', source: email, isUsed: false,
    });
    await newotp.save();

    return res.status(200).json({ msg: 'OTP sent' });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const sendPhoneOtp = async (req, res) => {
  try {
    const { phone } = req.body;
    const user = await User.findOne({ phone });
    if (req.query.verify) {
      if (user) {
        return res.status(400).json({ msg: 'User Already exists' });
      }
    } else if (!user) {
      return res.status(404).json({ msg: 'User Not exists' });
    }
    const otpCheck = await Otp.findOne({ generatedFrom: 'phone', source: phone }).sort({ createdAt: -1 }).limit(1);
    if (otpCheck && otpCheck.isUsed === false) {
      const now = dayjs();
      // eslint-disable-next-line max-len
      const twentyMinutesLater = new Date(otpCheck.createdAt.getTime() + (config.OTP_VALIDITY_IN_MINS * 60 * 1000));
      const newdate = new Date(now.format());
      if (newdate < (twentyMinutesLater)) {
        if (otpCheck.retryAttempts >= config.MAX_RETRY_ATTEMPT) {
          return res.status(400).json({ msg: 'Max resend limit reached. Pls wait for some time' });
        }
        otpCheck.retryAttempts += 1;
        await otpCheck.save();
        if (config.environment === 'production') {
          await sendOtpByPhone(phone, otpCheck.otp);
        }
        return res.status(200).json({ msg: 'OTP sent Again' });
      }
    }

    let otp;
    if (config.environment === 'production') {
      otp = Math.floor(100000 + Math.random() * 900000);
      await sendOtpByPhone(phone, otp);
    } else {
      otp = 111111;
    }
    const newotp = new Otp({
      otp, generatedFrom: 'phone', source: phone, isUsed: false,
    });

    await newotp.save();

    return res.status(200).json({ msg: 'OTP sent' });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const newProfile = async (req, res) => {
  try {
    const resp = await createProfile(req, req.user.id);
    return res.json(resp);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const editProfile = async (req, res) => {
  try {
    const profile = await Profile.findOne({ user: req.user.id });
    if (!profile) {
      return res.status(404).json({ msg: 'Profile Not found' });
    }
    const {
      shortBio,
      description,
      languages,
      introduction,
      introVideo,
      skills,
      organisations,
      expInYears,
      website,
      location,
      timeZone,
      bookSession,
      featured,
      socials,
      activeSocials,
      announcementEnabled,
    } = req.body;

    profile.shortBio = shortBio;
    profile.description = description;
    profile.languages = languages;
    profile.introduction = introduction;
    profile.introVideo = introVideo;
    profile.skills = skills;
    profile.organisations = JSON.parse(organisations);
    profile.expInYears = expInYears;
    profile.website = website;
    profile.location = location;
    profile.timeZone = timeZone;
    profile.bookSession = bookSession;
    profile.featured = featured;
    profile.socials = socials;
    profile.activeSocials = activeSocials;
    profile.announcementEnabled = announcementEnabled;

    if (req.files[0]) {
      const image = uploadToS3(req.files[0].path);
      profile.profilePic.url = ((await image).Location);
      profile.profilePic.size = req.files[0].size;
      profile.profilePic.type = req.files[0].mimetype;
      profile.profilePic.fileName = req.files[0].originalname;
    }

    await profile.save();

    return res.json(profile);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const viewProfile = async (req, res) => {
  try {
    const { userid } = req.params;
    //  const profile = await Profile.findOne({ user: userid }).populate({ path: 'user' });
    const query = [
      { $match: { user: mongoose.Types.ObjectId(userid) } },
      {
        $lookup: {
          from: 'sessions',
          let: { u_id: '$user' },
          pipeline: [
            {
              $match: {
                $expr:
                  { $eq: ['$mentorId', '$$u_id'] },
              },
            },
          ],
          as: 'sessionDetails',
        },
      },
      {
        $lookup: {
          from: 'users',
          let: { u_id: '$user' },
          pipeline: [
            {
              $match: {
                $expr:
                  { $eq: ['$_id', '$$u_id'] },
              },
            },
          ],
          as: 'userDetails',
        },
      },
      {
        $project: {
          _id: 1,
          user: 1,
          shortBio: 1,
          description: 1,
          languages: 1,
          introduction: 1,
          introVideo: 1,
          skills: 1,
          pastCompanies: 1,
          expInYears: 1,
          website: 1,
          location: 1,
          timeZone: 1,
          bookSession: 1,
          featured: 1,
          verified: 1,
          profilePic: 1,
          socials: 1,
          activeSocials: 1,
          announcementEnabled: 1,
          announcement: 1,
          announcementCTA: 1,
          sessionDetails: '$sessionDetails',
          userName: { $first: '$userDetails.name' },

        },
      },
    ];

    const profile = await Profile.aggregate(query);
    if (!profile) {
      return res.status(404).json({ msg: 'Profile Not found' });
    }
    return res.json(profile);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const checkEmailVerification = async (req, res) => {
  try {
    const user = await User.findById(req.params.userid);
    user.emaiVerified = true;
    return res.status(200).send('<h1>User Email Verified</h1>');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};

const getRating = async (req, res) => {
  try {
    const { entityType, entityID } = req.query;
    const rating = await Rating.findOne({ entityType, entityID, user: req.user.id });
    if (!rating) {
      return res.status(404).json({ msg: 'Not yet Rated' });
    }
    return res.status(200).json(rating);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return res.status(500).send('Server Error');
  }
};
module.exports = {
  // eslint-disable-next-line max-len
  verifyUser, userSignup, sendEmailVerification, newProfile, editProfile, checkEmailVerification, viewProfile, sendEmailOtp, sendPhoneOtp, getRating,
};
