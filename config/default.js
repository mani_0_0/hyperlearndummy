const dotenv = require('dotenv');

dotenv.config();
const config = {
  mongoURI: process.env.MONGO_URI.toString(),
  apiLink: process.env.API_LINK.toString(),
  MAX_RETRY_ATTEMPT: 3,
  OTP_VALIDITY_IN_MINS: 20,
  secret: 'mysecret',
  sessionSecret: 'secret',
  awsAccessKey: process.env.AWS_ACCESS_KEY.toString(),
  awsSecret: process.env.AWS_SECRET.toString(),
  awsBucketName: process.env.PUBLIC_BUCKET_NAME.toString(),
  environment: process.env.ENVIRONMENT.toString() || 'development',
  sendGridApiKey: process.env.SENDGRID_API_KEY.toString(),
  sendGridApiSender: process.env.SENDGRID_API_SENDER.toString(),
  textLocalApiKey: process.env.TEXTLOCAL_API_KEY.toString(),
  textLocalBaseUrl: process.env.TEXTLOCAL_BASE_URL.toString(),
  textLocalSenderId: process.env.TEXTLOCAL_SENDERID.toString(),
  redisClient: process.env.REDIS_CLIENT.toString(),
  redisPort: process.env.REDIS_PORT,
  redisPassword: process.env.REDIS_PASSWORD.toString(),
  razorpayId: process.env.RAZORPAY_ID.toString(),
  razorpayKeySecret: process.env.RAZORPAY_KEY_SECRET.toString(),
  razorpayWebhookSecret: process.env.RAZORPAY_WEBHOOK_SECRET.toString(),
  googleClientId: process.env.GOOGLE_CLIENT_ID.toString(),
  googleClientSecret: process.env.GOOGLE_CLIENT_SECRET.toString(),
  redirectUri: process.env.REDIRECT_URI.toString(),
  successOAuthRedirect: process.env.SUCCESS_OAUTH_REDIRECT.toString(),
};

module.exports = { config };
