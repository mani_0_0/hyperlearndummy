const Enrolled = require('../models/Enrolled');
const Course = require('../models/Course');
const Message = require('../models/Message');
const User = require('../models/User');

const createEnrollment = async (user, course) => {
  try {
    const enrollmentCheck = await Enrolled.findOne({ user, course }).populate({ path: 'user', select: 'name' });
    if (enrollmentCheck) {
      return { msg: 'user already enrolled' };
    }

    const Courses = await Course.findById(course).select(['publish', 'user', 'heading']);

    if (!Courses) {
      return { msg: 'Course Not found' };
    }
    if (Courses.user.toString() === user) {
      return { msg: 'Mentor cannot be enrolled' };
    }

    if (Courses.publish !== 'published') {
      return { msg: 'Course not Yet Published' };
    }

    if (!Courses) {
      return { msg: 'Course not found' };
    }
    const student = await User.findById(user).select('name');
    const enrollment = new Enrolled({
      user,
      course,
      instructorId: Courses.user.toString(),
    });

    await enrollment.save();

    const newMessage = new Message({
      senderId: Courses.user.toString(),
      receiverId: user,
      courseId: course,
      msgType: 'system',
      enrollmentId: enrollment.id,
    });
    newMessage.source.msg = `${student.name} has enrolled in ${Courses.heading}`;
    await newMessage.save();

    return enrollment;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return 'Server Error';
  }
};

module.exports = { createEnrollment };
