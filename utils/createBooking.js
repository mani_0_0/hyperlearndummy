/* eslint-disable no-underscore-dangle */
const DateTime = require('node-datetime');
const { google } = require('googleapis');
const Promise = require('bluebird');

const { OAuth2 } = google.auth;
const { config } = require('../config/default');
const Booking = require('../models/Booking');
const Session = require('../models/Session');
const oAuthToken = require('../models/oAuthToken');

const oAuth2Client = new OAuth2(
  config.googleClientId,
  config.googleClientSecret,
  config.redirectUri,
);

const createBooking = async (body, userId) => {
  try {
    const {
      mentorId, startTime, endTime, status, orderId, attendees, comments, sessionId,
    } = body;
    const formatStartTime = DateTime.create(startTime);
    const formatEndTime = DateTime.create(endTime);

    const newBooking = new Booking({
      mentorId,
      userId,
      // eslint-disable-next-line no-underscore-dangle
      startTime: formatStartTime._now,
      // eslint-disable-next-line no-underscore-dangle
      endTime: formatEndTime._now,
      status,
      orderId,
      comments,
    });
    const auth = await oAuthToken.findOne({ userId: mentorId });

    if (!auth) {
      return { msg: 'Please connect calender' };
    }
    oAuth2Client.setCredentials(auth.oAuthtoken);
    const calendar = google.calendar({ version: 'v3', auth: oAuth2Client });

    const session = await Session.findById(sessionId);

    await newBooking.save();
    const bookingMade = calendar.events.insert({
      calendarId: 'primary',
      conferenceDataVersion: 1,
      resource: {
        summary: session.name,
        description: session.description,
        start: {
          dateTime: formatStartTime._now.toISOString(),
          // timeZone: 'utc',
        },
        end: {
          dateTime: formatEndTime._now.toISOString(),
          // timeZone: 'utc',
        },
        // attendees: [{ email: '2019284@iiitdmj.ac.in' }],
        attendees: [
          { email: attendees },
        ],
        conferenceData: {
          createRequest: {

            conferenceSolutionKey: {
              type: 'hangoutsMeet',
            },
            requestId: newBooking.id.toString(),
          },
        },
      },
      sendUpdates: 'all',
    });

    const temp = await Promise.all([bookingMade]);
    const result = {};
    if (temp[0]) {
      // eslint-disable-next-line prefer-destructuring
      result.googleBooking = temp[0];
    }

    result.schedulerBooking = newBooking;

    return result;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return 'Server Error';
  }
};

module.exports = { createBooking };
