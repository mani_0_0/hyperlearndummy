const Session = require('../models/Session');

const createSession = async (body, mentorId) => {
  try {
    const {
      duration, name, description, markedPrice, price, sessionType,
    } = body;
    const newSession = new Session({
      mentorId,
      name,
      description,
      duration,
      markedPrice,
      price,
      sessionType,
    });

    await newSession.save();
    return newSession;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    throw new Error('Server Error');
  }
};

module.exports = { createSession };
