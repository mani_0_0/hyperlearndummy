const axios = require('axios');
const { config } = require('../config/default');

const sendMessage = async (phone, text) => {
  try {
    // eslint-disable-next-line radix
    const uriComponent = encodeURIComponent(`OTP to access your account is ${text}- Abhijeet from Hyperlearn`);
    const url = `${config.textLocalBaseUrl}/send/?apiKey=${config.textLocalApiKey}=&sender=${config.textLocalSenderId}&numbers=91${phone.toString()}&message=${uriComponent.toString()}`;
    if (config.environment === 'production') {
      const res = await axios.get(url);
      // eslint-disable-next-line no-console
      if (res.status === 200) {
        // eslint-disable-next-line no-console
        console.log('Otp sent successfully');
        return;
      }
      // eslint-disable-next-line no-console
      console.log('Otp send failed');
      return;
    }
    // eslint-disable-next-line no-console
    console.log('Otp Sent');
    return;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
  }
};

module.exports = { sendMessage };
