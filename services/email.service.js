/* eslint-disable no-unused-vars */
const sgMail = require('@sendgrid/mail');
const { config } = require('../config/default');

sgMail.setApiKey(config.sendGridApiKey);
// eslint-disable-next-line consistent-return
const sendEmail = (to, subject, text, html) => {
  try {
    const msg = {
      to, // Change to your recipient
      from: config.sendGridApiSender, // Change to your verified sender
      subject,
      text,
      html: text,
    };
    sgMail
      .send(msg)
      .then(() => {
        // eslint-disable-next-line no-console
        console.log('Email sent');
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error.response.body);
      });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('Something Failed');
    return err;
  }
};

module.exports = { sendEmail };
