const axios = require('axios');

const autocompleteCompanySearch = async (name) => {
  const companies = await axios.get(`https://autocomplete.clearbit.com/v1/companies/suggest?query=${name}`);

  return companies;
};

module.exports = { autocompleteCompanySearch };
