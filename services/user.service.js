const { config } = require('../config/default');
const { sendEmail } = require('./email.service');
const { sendMessage } = require('./message.service');

const sendOtpToUserByEmail = async (email, otp) => {
  try {
    const subject = 'Otp from Hyperlearn';
    const text = `Hi, your otp is:${otp.toString()}`;

    await sendEmail(email, subject, text, '');
    // eslint-disable-next-line no-console
    console.log('Email OTP Sent');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
};

const sendEmailVerificationLink = (email, userId) => {
  const verificationLink = `<h1>${config.apiLink}users/email/verify/user/${userId}</h1>`;
  const subject = 'Email Verification';
  const text = ` Verify Link ${config.apiLink}users/email/verify/user/${userId}</h1>`;
  sendEmail(email, subject, text, verificationLink);

  // eslint-disable-next-line no-console
  console.log('Email verify Sent');
};

const sendOtpByPhone = async (phone, otp) => {
  try {
    await sendMessage(phone.toString(), otp.toString());
    // eslint-disable-next-line no-console
    console.log('Otp sent');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
};
module.exports = { sendOtpToUserByEmail, sendEmailVerificationLink, sendOtpByPhone };
