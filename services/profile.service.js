const Profile = require('../models/Profile');
const User = require('../models/User');
const { uploadToS3 } = require('./uploadToS3.service');

const createProfile = async (req, userId) => {
  try {
    const profile = await Profile.findOne({ user: userId });
    if (profile) {
      return { msg: 'Profile Already exist' };
    }
    const {
      shortBio,
      description,
      languages,
      introduction,
      introVideo,
      skills,
      organisations,
      expInYears,
      website,
      location,
      timeZone,
      bookSession,
      featured,
      socials,
      activeSocials,
      announcementEnabled,
    } = req.body;
    const parsedOrganisation = JSON.parse(organisations);
    const newProfile = new Profile({
      user: userId,
      shortBio,
      description,
      languages,
      introduction,
      introVideo,
      skills,
      organisations: parsedOrganisation,
      expInYears,
      website,
      location,
      timeZone,
      bookSession,
      featured,
      socials,
      activeSocials,
      announcementEnabled,
    });

    if (req.files[0]) {
      const image = uploadToS3(req.files[0].path);
      // eslint-disable-next-line no-console
      newProfile.profilePic.url = ((await image).Location);
      newProfile.profilePic.size = req.files[0].size;
      newProfile.profilePic.type = req.files[0].mimetype;
      newProfile.profilePic.fileName = req.files[0].originalname;
    }
    const user = await User.findById(req.user.id);

    await newProfile.save();
    user.profile = newProfile.id;
    user.user_type = 'mentor';
    await user.save();

    return newProfile;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    throw new Error('Server Error');
  }
};

module.exports = { createProfile };
