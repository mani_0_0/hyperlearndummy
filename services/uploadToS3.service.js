const AWS = require('aws-sdk');
const fs = require('fs');
const { config } = require('../config/default');

const ID = config.awsAccessKey;
const SECRET = config.awsSecret;

const s3 = new AWS.S3({
  accessKeyId: ID,
  secretAccessKey: SECRET,
});

async function uploadToS3(fileName) {
  try {
    const fileContent = fs.readFileSync(fileName);

    // Setting up S3 upload parameters
    const params = {
      Bucket: config.awsBucketName,
      Key: fileName, // File name you want to save as in S3
      Body: fileContent,
    };

    // Uploading files to the bucket
    const s3upload = s3.upload(params).promise();

    const image = await s3upload;
    return image;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.message);
    return 'Error';
  }
  // Read content from the file
}

module.exports = { uploadToS3 };
