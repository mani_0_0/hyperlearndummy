const Availability = require('../models/Availability');

const createAvailability = async (body, userId) => {
  try {
    const {
      isAvailable, bookingAllowedBefore, bookingAllowedTill, timeZone, days,
    } = body;

    const availability = await Availability.findOne({ userId });
    if (availability) {
      availability.isAvaialable = isAvailable;
      availability.bookingAllowedBefore = bookingAllowedBefore;
      availability.bookingAllowedTill = bookingAllowedTill;
      availability.timeZone = timeZone;
      availability.days = days;
      availability.isAvailable = isAvailable;
      await availability.save();
      return availability;
    }

    const newAvailability = new Availability({
      isAvailable,
      bookingAllowedBefore,
      bookingAllowedTill,
      timeZone,
      days,
      userId,
    });

    await newAvailability.save();
    return newAvailability;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    throw new Error('Server Error');
  }
};

module.exports = { createAvailability };
