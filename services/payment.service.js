const shortid = require('shortid');
const Razorpay = require('razorpay');

const { config } = require('../config/default');

const razorpay = new Razorpay({
  key_id: config.razorpayId,
  key_secret: config.razorpayKeySecret,
});

const razorpayPayment = async (amount) => {
  const options = {
    amount,
    currency: 'INR',
    receipt: shortid.generate(),
    payment_capture: 1,
  };

  try {
    const response = await razorpay.orders.create(options);
    // eslint-disable-next-line no-console
    console.log(response);
    return response;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
    return 'Error';
  }
};

module.exports = { razorpayPayment };
